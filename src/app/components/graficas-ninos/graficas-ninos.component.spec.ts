import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficasNinosComponent } from './graficas-ninos.component';

describe('GraficasNinosComponent', () => {
  let component: GraficasNinosComponent;
  let fixture: ComponentFixture<GraficasNinosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraficasNinosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficasNinosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
