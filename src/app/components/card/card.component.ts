import { Component, OnInit } from '@angular/core';
import { Personal } from 'src/app/interfaces/usuario.interface';
import {  ModulesService } from '../../services/modules/modules.service';
import { Router } from '@angular/router';
/* import * as confetti from 'canvas-confetti'; */
/* import { randomInRange } from '../../functions/storage.funcion'; */
/* require('canvas-confetti'); */

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  public pagina = 0;
  public totalAmbulancia: string;
  public usuario: Personal;
  public modules:any[]=[];

  constructor(public router: Router,
              public modulesService: ModulesService){}

  ngOnInit(): void {
    this.getRoleLocalStorage();
  }

  getRoleLocalStorage(){
    // SETEA LOS DATOS DEL USURAIO EN EL LOCALSTORAGE
    this.usuario = JSON.parse(localStorage.getItem('usuario'));
    if( this.usuario == null || this.usuario == undefined){
      return;
    }else{
      this.getModuleByRole();
    }
  }

  getModuleByRole(){
    this.modulesService.getModules( this.usuario._id ).subscribe( (data:any)  => {
      //  OBTENEMOS TODOS LOS MODULOS DEL USUARIO
      this.modules = data.data;
      //La api cambio la forma de la respuesta
      this.totalAmbulancia = data.data.results;
    })
  }
  
}
