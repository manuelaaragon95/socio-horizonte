import { Component, OnInit,Input} from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { RecetasService } from 'src/app/services/recetas/recetas.service';



@Component({
  selector: 'app-tablas-consultas',
  templateUrl: './tablas-consultas.component.html',
  styleUrls: ['./tablas-consultas.component.css']
})
export class TablasConsultasComponent implements OnInit {

  @Input() estudiosPendientes: string;

  public consultas:any []=[];
  public recetasConEstudios:any[] =[];
  public pagina = 0;
  public total: string;
  public sede = '';

  constructor(public _consultaService: ConsultaService,
              private _recetaService: RecetasService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.sede = localStorage.getItem('cede');
    this.spinner.show();
    if( this.estudiosPendientes == 'consultas'){
      this.obtenerCosnultas();
    }else if( this.estudiosPendientes == 'estudios'  ){
      this.obtenerRecetas();
    }
  }
  obtenerCosnultas(){
    this._consultaService.verConsultasRecepcion(this.sede)
    .subscribe( (data) =>   {
      this.setConsultas(data['data'].reverse());
    });
  }

  setConsultas(consultas:any){
    consultas.forEach(element => {
      if (element.paciente != null) {
        this.consultas.push(element)
      }
    });
    this.total=consultas.results;
    this.spinner.hide();
  }


  obtenerRecetas(){
    this._recetaService.verRecetasEmitidasSede(this.sede)
    .subscribe(  (data) => {
      data['data'].forEach(element => {
        if (element.idPaciente != null) {
          this.recetasConEstudios.push(element)
        }
      });
      this.recetasConEstudios.reverse();
      this.spinner.hide();
    });
  }

}
