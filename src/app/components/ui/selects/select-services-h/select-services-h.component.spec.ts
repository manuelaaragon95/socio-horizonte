import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectServicesHComponent } from './select-services-h.component';

describe('SelectServicesHComponent', () => {
  let component: SelectServicesHComponent;
  let fixture: ComponentFixture<SelectServicesHComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectServicesHComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectServicesHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
