import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-select-services-h',
  templateUrl: './select-services-h.component.html',
  styleUrls: ['./select-services-h.component.css']
})
export class SelectServicesHComponent implements OnInit {

  public sede = '';
  constructor(
    public _router: Router
  ) { }

  ngOnInit(): void {
    this.sede = localStorage.getItem('cede')
  }
  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    if(servicio == 'consulta' && this.sede != 'CTLA01'){
      this._router.navigate([ `/consulta`]);
    }else{
      this._router.navigate([ `/serviciosInt/${servicio}`]);
    }
  }
}
