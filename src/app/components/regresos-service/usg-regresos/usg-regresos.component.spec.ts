import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsgRegresosComponent } from './usg-regresos.component';

describe('UsgRegresosComponent', () => {
  let component: UsgRegresosComponent;
  let fixture: ComponentFixture<UsgRegresosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsgRegresosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsgRegresosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
