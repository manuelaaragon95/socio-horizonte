import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LabsRegresosComponent } from './labs-regresos.component';

describe('LabsRegresosComponent', () => {
  let component: LabsRegresosComponent;
  let fixture: ComponentFixture<LabsRegresosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LabsRegresosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LabsRegresosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
