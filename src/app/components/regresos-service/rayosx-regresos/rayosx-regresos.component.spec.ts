import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RayosxRegresosComponent } from './rayosx-regresos.component';

describe('RayosxRegresosComponent', () => {
  let component: RayosxRegresosComponent;
  let fixture: ComponentFixture<RayosxRegresosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RayosxRegresosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RayosxRegresosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
