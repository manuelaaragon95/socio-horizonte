import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatoRegresosComponent } from './pato-regresos.component';

describe('PatoRegresosComponent', () => {
  let component: PatoRegresosComponent;
  let fixture: ComponentFixture<PatoRegresosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatoRegresosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatoRegresosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
