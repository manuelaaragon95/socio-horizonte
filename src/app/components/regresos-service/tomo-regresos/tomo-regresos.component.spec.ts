import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TomoRegresosComponent } from './tomo-regresos.component';

describe('TomoRegresosComponent', () => {
  let component: TomoRegresosComponent;
  let fixture: ComponentFixture<TomoRegresosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TomoRegresosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TomoRegresosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
