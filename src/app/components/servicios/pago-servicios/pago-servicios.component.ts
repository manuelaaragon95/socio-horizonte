// IMPORTACIÓN DE LOS COMPONENTES PARA PAGOS
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';

import Carrito from 'src/app/clases/carrito/carrito';
/* import {SEDE} from 'src/app/config/conf'; */
//import {} from '../../../services/'
import Dates from 'src/app/clases/dates/dates';
import PacienteStorage from 'src/app/clases/pacientes/pacientes.class';
import { PagosService } from 'src/app/services/pagos/pagos.service';
import { getDataStorage } from 'src/app/functions/storage/storage.functions';
import Tickets from 'src/app/clases/tickets/tickets.class';
import { eliminarStorage } from 'src/app/functions/storage/pacienteIntegrados';
// import {CarritoMembresiaStorage} from 'src/app/clases/carritoMembresía/storageCarritoMembresia.class';
// import { eliminarTodoPedido, getDataStorage } from 'src/app/functions/storage.funcion';
import { USGService } from '../../../services/usg/usg.service';
import { XrayService } from '../../../services/Rayos-x/xray.service'
import { NgxSpinnerService } from 'ngx-spinner';
import { SedesService } from 'src/app/services/sedes/sedes.service';
import { calcDeparmentsPorcentaje } from 'src/app/clases/helpers/filterNamePaciente';
import { PedidosSedesService } from 'src/app/services/pedidosSedes/pedidos-sedes.service';
import { alerta2 } from 'src/app/clases/helpers/alerta';
import { WsLoginService } from 'src/app/services/sockets/ws-login.service';

@Component({
  selector: 'app-pago-servicios',
  templateUrl: './pago-servicios.component.html',
  styleUrls: ['./pago-servicios.component.css']
})
export class PagoServiciosComponent implements OnInit {
  public fecha = "";

  public paciente = {

    nombrePaciente:'',
    apellidoPaterno:'',
    apellidoMaterno: '',
    edad:0,
    genero:'',
    direccion:{
      callePaciente:""
    },
    id:'',
    membresiaActiva:false,
    sede:'',
    _id:""
  }
  public IVaDEl16 = 0;
  public positionYPDF = 100;

  public infoVenta = {
    paciente:"",
    nombrePaciente:"",
    vendedor:"",
    fecha:"",
    hora:"",
    estudios:[],
    efectivo:false,
    transferencia: false,
    tarjetCredito:false,
    tarjetaDebito:false,
    doctorQueSolicito:"",
    anticipo: false,
    sinpago:false,
    montoSinpago:0,
    montoEfectivo:0,
    montoAnticipo: 0,
    montoTarjteCredito:0,
    montoTarjetaDebito:0,
    montoTranferencia: 0,
    sede:"",
    socioSede:"",
    totalCompra:0,
    prioridad:"",
    compraConMembresia:true,
    status: "",
    ganancia:0,
    gananciaPromedio:0,
    idVenta:"",
  }

  public btnValidacion=true;

  public totalSinMembresia = 0;

  public carrito = {
    totalSin: 0,
    totalCon: 0,
    items: []
  };

  public totalConIva=0;
  public totalCarritoMembresia = 0;

  public pedidosLaboratorios = {

    estudios:[],
    idPaciente:"",
    fecha:"",
    hora:"",
    medicoQueSolicita:"",
    sede:"",
    prioridad:"Programado"

  }

  public pedidosUltrasonido = {
    idPaciente:"",
    estudios:[],
    fecha:"",
    hora:"",
    medicoQueSolicita:"",
    sede:"",
    prioridad:"Programado"
  }

  public pedidosRayox = {
    idPaciente:"",
    estudios:[],
    fecha:"",
    hora:"",
    medicoQueSolicita:"",
    sede:"",
    prioridad:"Programado"
  }
  public cede='';
  public rangoAnticipo= 0;
  public porcentajeSocio = 0;
  public porcentajeServicio =0;

  constructor(
    private _xrayService : XrayService,
    private _ultrasonidoService: USGService,
    private _pedidosSedes :PedidosSedesService,
    private _sedes: SedesService,
    private _pagoServicios:PagosService,
    private _router: Router,
    private spinner: NgxSpinnerService,
    private _wsLoginService: WsLoginService
    //private _ultrasonidoService: USGService
  ) { }

  ngOnInit(): void {
    this.obtenerSede();
    this.obtenerPacienteStorage();
    this.obtenerCarrito();
    this.obtenerTotalCarrito();
    this.getPorcentajeServicio();
  }

  obtenerSede(){
    this.cede = localStorage.getItem('cede');
  }

  obtenerTotalCarrito(){
    this.totalSinMembresia = this.carrito.totalSin;
    this.totalCarritoMembresia= this.carrito.totalCon;
  }

  setDatesVenta(){
    const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;

    /* this.fecha = this.fecha = moment().format('l');; */

    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    this.infoVenta.paciente = this.paciente._id;
    if(this.infoVenta.paciente == undefined){
      this.infoVenta.paciente = this.paciente.id;
    }
    this.infoVenta.sede = this.cede;
    this.infoVenta.prioridad = "Programado"
    if(this.infoVenta.fecha == ''){
      this.infoVenta.fecha = dates.getDate();
    }
    /* if(this.paciente.membresiaActiva){
      this.carrito.items.push();
    } */
    this.infoVenta.estudios = this.carrito.items;
    if(this.paciente.membresiaActiva){
      this.infoVenta.totalCompra = this.carrito.totalCon;
    }else{
      this.infoVenta.totalCompra = this.carrito.totalSin;
      this.infoVenta.compraConMembresia= false
    }
    this.infoVenta.status="Pendiente"
    if (this.cede == 'CTLA01') {
      this.infoVenta.status="Pagado"
    }

  }

  setDatesPedidos (){
    //creamos un nuevo json para todos los pedidos que se hagan conforme a la venta
    this.carrito.items.forEach(  (element) => {
      let jsonToPedidos = { ... this.infoVenta}
     //  console.log(element);
      
      Object.assign( jsonToPedidos, {idPaciente: this.infoVenta.paciente} );
     //comparamos que si la sede es diferenre de tlayacapan retorne otra respuesta 
     Object.assign( jsonToPedidos, {idServicio: element.idEstudio} );
     Object.assign( jsonToPedidos, {nombreServicio: element.name} );

     //eliminamos los datos que estan de mas
     delete jsonToPedidos.estudios
     delete jsonToPedidos.montoEfectivo
     delete jsonToPedidos.montoTarjetaDebito
     delete jsonToPedidos.montoTarjteCredito
     delete jsonToPedidos.montoTranferencia
     delete jsonToPedidos.efectivo
     delete jsonToPedidos.tarjetCredito
     delete jsonToPedidos.tarjetaDebito
     delete jsonToPedidos.transferencia
     delete jsonToPedidos.compraConMembresia
     delete jsonToPedidos.nombrePaciente
     delete jsonToPedidos.status
     //peticion que se lanza para los nuevo estudios
     this.requestPedidos(jsonToPedidos);
    });
   //pedios de los estudios 
  }

  requestPedidos( jsonPedidios ) {
    if(jsonPedidios.nombreServicio == 'laboratorio'){
      jsonPedidios.manzo = true;
    }
    this._pedidosSedes.postANewPedidoNew( jsonPedidios )
    .subscribe( (data:any) => {
      if(data.ok){
        const { _id, nombreServicio } = data.data;
        if(nombreServicio == 'LABORATORIO'){
          this._wsLoginService.enviarLaboratorios(_id)
        }
      }
    })
  }

  obtenerPacienteStorage(){

    const pacienteStorage = new PacienteStorage();
    this.paciente = pacienteStorage.verPacienteConMembresia();
  }

  obtenerCarrito(){
    let carritoSinMembresia = new Carrito();
    this.carrito = carritoSinMembresia.obtenerSotorageCarrito();
    this.rangoAnticipo = 
    this.totalSinMembresia = this.carrito.totalSin;
    this.totalCarritoMembresia= this.carrito.totalCon;
  }



  validarBoton(valor){
    if( valor == 0  ){
      this.btnValidacion = false;
    }
  }

  validar(valor){
    if(valor != 0){
      this.btnValidacion = false;
    }
  }


  eliminarCarritoSinMembresia( item  ){
    let carritoMembresia = new Carrito();
    carritoMembresia.eliminar( item );
    this.obtenerCarrito();
    this.calcularAnticipo(2);
  }


  calcularNuevoTotalEfectivo(){
    if(this.paciente.membresiaActiva){
      if(this.carrito.totalCon < this.infoVenta.montoEfectivo){
        /* alert("monto mayor") */
        alerta2('warning', 'Monto mayor');
      }else{
        if(this.infoVenta.anticipo){
          this.infoVenta.anticipo = false
          this.obtenerCarrito();
          this.infoVenta.montoAnticipo=0
        }
        this.totalCarritoMembresia = this.carrito.totalCon - this.infoVenta.montoEfectivo;
        this.infoVenta.compraConMembresia = true;
        this.validarBoton(this.totalCarritoMembresia);
      }
    }else{
      if(this.carrito.totalSin < this.infoVenta.montoEfectivo){
        /* alert("monto mayor") */
        alerta2('warning', 'Monto mayor');
      }else{
        if(this.infoVenta.anticipo){
          this.infoVenta.anticipo = false
          this.obtenerCarrito();
          this.infoVenta.montoAnticipo=0
        }
        this.totalSinMembresia = this.carrito.totalSin - this.infoVenta.montoEfectivo;
        this.infoVenta.compraConMembresia = false;
        this.validarBoton(this.totalSinMembresia);
      }
    }
  }

  calcuarMontoTarjetaDebito(){
    if(this.paciente.membresiaActiva){
    if(this.infoVenta.montoTarjetaDebito > this.totalCarritoMembresia){
      alerta2('warning', 'Monto mayor');
    }else{
      this.totalCarritoMembresia = this.totalCarritoMembresia  - this.infoVenta.montoTarjetaDebito;
      this.validarBoton(this.totalCarritoMembresia);
    }
    }else{
    if(this.infoVenta.montoTarjetaDebito > this.totalSinMembresia){
      alerta2('warning', 'Monto mayor');
    }else{
      this.totalSinMembresia = this.totalSinMembresia  - this.infoVenta.montoTarjetaDebito;
      this.validarBoton(this.totalSinMembresia);
    }
    }
  }

  calcularMontoTarjetaCredito(){
    if(this.paciente.membresiaActiva){
      if(this.infoVenta.montoTarjteCredito > this.totalCarritoMembresia){
        alerta2('warning', 'Monto mayor');
      }else{
        this.totalCarritoMembresia = this.totalCarritoMembresia  - this.infoVenta.montoTarjteCredito;
        this.validarBoton(this.totalCarritoMembresia);
      }
    }else{
      if(this.infoVenta.montoTarjteCredito > this.totalSinMembresia){
        alerta2('warning', 'Monto mayor');
      }else{
        this.totalSinMembresia = this.totalSinMembresia  - this.infoVenta.montoTarjteCredito;
        this.validarBoton(this.totalSinMembresia);
      }
    }
  }

  calcularMontoSinPago(){
    this.btnValidacion = false;
      if(this.infoVenta.montoSinpago > this.totalSinMembresia){
        alerta2('warning', 'Monto mayor');
      }else{
        this.totalSinMembresia = this.totalSinMembresia - this.infoVenta.montoAnticipo;
        this.validar(this.totalSinMembresia);
        this.calcularAnticipoSin(this.totalSinMembresia);
      }
  }
//TODO: arreglar el problema del pago sin cobro
  calcularMontoAnticipo(){
    this.btnValidacion = false;
    if(this.paciente.membresiaActiva){
      if(this.infoVenta.montoAnticipo > this.totalCarritoMembresia){
        alerta2('warning', 'Monto mayor');
      }else{
        if(this.infoVenta.efectivo){
          this.infoVenta.efectivo = false
          this.obtenerCarrito();
          this.infoVenta.montoEfectivo= 0
        }
        this.totalCarritoMembresia = this.totalCarritoMembresia - this.infoVenta.montoAnticipo;
        this.validarBoton(this.totalCarritoMembresia);
        this.calcularAnticipo(this.totalCarritoMembresia);
      }
    }else{
      if(this.infoVenta.montoAnticipo > this.totalSinMembresia){
        alerta2('warning', 'Monto mayor');
      }else{
        if(this.infoVenta.efectivo){
          this.infoVenta.efectivo = false
          this.obtenerCarrito();
          this.infoVenta.montoEfectivo= 0
        }
        this.totalSinMembresia = this.totalSinMembresia - this.infoVenta.montoAnticipo;
        this.validarBoton(this.totalSinMembresia);
        this.calcularAnticipo(this.totalSinMembresia);
      }
    }
  }

  calcularAnticipo(monto){
    let mont = 0;
    this.infoVenta.montoAnticipo = 0;
    this.carrito.items.forEach((element) => {
      if(element.name == 'laboratorio'){
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }
      }else if(element.name == 'ultrasonidos'){
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }
      }else if(element.name == 'xray'){
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }
      }else if(element.name == 'endoscopia'){
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }
      }else if(element.name == 'otros'){
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }
      }else{
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.montoAnticipo = this.infoVenta.montoAnticipo + mont;
        }
      }
    })
  }

  calcularAnticipoSin(monto){
    this.infoVenta.montoAnticipo= (0 * monto)/100
  }


  generarTicket(folio){

    const tickets = new Tickets();
    tickets.printTicketSinMembresia( this.paciente, this.carrito ,  this.infoVenta, folio );

  }


calcularMontoTrajetaDebito(){
  if(  this.infoVenta.tarjetaDebito ){
      this.totalCarritoMembresia = this.totalCarritoMembresia - this.infoVenta.montoTarjteCredito;
    this.validarBoton(  this.totalCarritoMembresia );
  }
}



agregarIva(){
  // agregamos la comision
   if(this.paciente.membresiaActiva){
     let iva = 0.0;

     if( this.infoVenta.tarjetCredito  ){

       iva = 1.9;

     }else if( this.infoVenta.tarjetaDebito ){

       iva = 2.5;
     }


       let totalIva = (( this.totalCarritoMembresia * iva ) / 100);
       this.totalConIva =  this.totalCarritoMembresia + totalIva;
       this.totalCarritoMembresia =  Math.round(this.totalConIva);
   }else{
     let iva = 0.0;

     if( this.infoVenta.tarjetCredito  ){

       iva = 1.9;

     }else if( this.infoVenta.tarjetaDebito ){

       iva = 2.5;
     }


       let totalIva = (( this.totalSinMembresia * iva ) / 100);
       this.totalConIva =  this.totalSinMembresia + totalIva;
       this.totalSinMembresia =  Math.round(this.totalConIva);
   }
}

calcularGanancia(){
  let mont = 0;
    this.infoVenta.ganancia = 0;
    this.carrito.items.forEach((element) => {
      if(element.name == 'laboratorio'){
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }
      }else if(element.name == 'ultrasonidos'){
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }
      }else if(element.name == 'xray'){
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }
      }else if(element.name == 'endoscopia'){
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }
      }else if(element.name == 'otros'){
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }
      }else{
        if(this.paciente.membresiaActiva){
          mont= (element.utilidad * element.precioCon)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }else{
          mont= (element.utilidad * element.precioSin)/100
          this.infoVenta.ganancia = this.infoVenta.ganancia + mont;
        }
      }
    })
}

async getPorcentajeServicio() {
  this.carrito.items.forEach(( element: any, index) => {
      // console.log(element)
    this._sedes.getPorcentajeBySede(this.cede, element.name)
    .subscribe((resp)=>{
      this.setDatos(resp['data'],index)
    })
  });
}

setDatos(datos:any, index){
  this.infoVenta.estudios.push(datos)
  this.porcentajeSocio = calcDeparmentsPorcentaje(datos)
  this.infoVenta.gananciaPromedio += this.sumPorcentajes(calcDeparmentsPorcentaje(datos));
  if(index == this.carrito.items.length-1){
    let valor = this.infoVenta.gananciaPromedio / this.carrito.items.length;
    this.infoVenta.gananciaPromedio = valor;
  }
}

sumPorcentajes(porcentaje:any){
  this.porcentajeServicio += porcentaje;
  return this.porcentajeServicio
}

enviarPedido(){
  this.spinner.show();
  this.setDatesVenta();
  this.calcularGanancia();
  this.infoVenta.compraConMembresia = this.paciente.membresiaActiva;
  let sede = localStorage.getItem('cede');
  if (sede == 'CTLA01') {
    this.infoVenta.socioSede = this.paciente.sede;
    this._pagoServicios.agregarPedido( this.infoVenta )
    .subscribe( async(data:any) => {
      if(  data['ok'] ){
        if(this.infoVenta.sede != 'TLYC01'){
          let gan = {
            ganancias:this.infoVenta.ganancia
          }
          let info = Object.assign(this.infoVenta,gan)
          this._pedidosSedes.ventas(info).subscribe((resp:any)=>{ console.log(resp) });
        }
        if(data.data._id) {
          this.infoVenta.idVenta = await data.data._id;
          this.setDatesPedidos();
        }
        this.generarTicket(data['data']['folio']);
        alerta2('success', 'Venta exitosa');
        // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
        // seteamos las fechas
        eliminarStorage(); 
        let validarConsultas = this.verificarConsulta()
        if (validarConsultas >= 1) {
          this.spinner.hide(); 
          this._router.navigateByUrl('/consulta');
        }else{
          const eliminarPaciente = new PacienteStorage();
          eliminarPaciente.removePacienteStorage(); 
          this.spinner.hide(); 
          this._router.navigateByUrl('/'); 
        }
      }
    });
  }else{
    this._pagoServicios.venta( this.infoVenta)
    .subscribe( (data) => {
      if(  data['ok'] ){
        if(this.infoVenta.sinpago == false){
          this.generarTicket(data['data'].folio);
        }
          // se crea la tabla de las ventas
          alerta2('success', 'Venta exitosa');
          // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS
          // seteamos las fechas
  
            eliminarStorage();
  
            const eliminarPaciente = new PacienteStorage();
            eliminarPaciente.removePacienteStorage();
            this.spinner.hide();
            this._router.navigateByUrl('/consulta');
        }
    });
  }

}

verificarConsulta(){
  let contadorConsultas = 0;
  this.infoVenta.estudios.forEach(element =>{
    if (element.name == 'consulta') {
      contadorConsultas+=1
    }
  })
  return contadorConsultas;
}

eliminar(i){
   const eliminarItem = new Carrito;
  eliminarItem.eliminar(i);
  this.obtenerCarrito();
  this.obtenerTotalCarrito();
  this.calcularMontoAnticipo();
}


calcularIva(){
  this.IVaDEl16 = ((this.carrito.totalSin  * 16 ) / 100);
  return this.IVaDEl16;
}



}



interface items {

  nombreEstudio : string
  precioCon: string
  precioSin: string
  idEstudio: string
  name: string
}


