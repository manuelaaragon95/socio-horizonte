import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodosServComponent } from './todos-serv.component';

describe('TodosServComponent', () => {
  let component: TodosServComponent;
  let fixture: ComponentFixture<TodosServComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodosServComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TodosServComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
