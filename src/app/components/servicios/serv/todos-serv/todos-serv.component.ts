import { Component, Input, OnInit } from '@angular/core';
import { ServiciosService } from '../../../../services/serviciosInt/servicios.service';
import { Router, ActivatedRoute} from '@angular/router';
/* import { gaurdarCotizacion } from '../../../../functions/storage.funcion';*/
import { getCarritoStorage } from '../../../../functions/storage/pacienteIntegrados';
import  {  getDataStorage   } from '../../../../functions/storage/storage.functions';
import swal from 'sweetalert';
import PacienteStorage from 'src/app/clases/pacientes/pacientes.class';
import  Carrito  from '../../../../clases/carrito/carrito.class';
import { filterServicios } from 'src/app/clases/buscarServicos/buscarServicos';

@Component({
  selector: 'app-todos-serv',
  templateUrl: './todos-serv.component.html',
  styleUrls: ['./todos-serv.component.css']
})
export class TodosServComponent implements OnInit {

  // data de los servicios
  @Input() serviceSi: any [] = [];
  public ambulanciaSI: any [] = [];
  public totalAmbulancia: string;
  //forma: FormGroup;
  public buscar = {
    estudio:''
  }
  public pagina = 0;
  public showTableAmbulanacia = true;
  public showAmbulanacia = false;
  @Input() showVista= true;
  public searching = false;
  public servicios ="";

  public membresia = false;
  public preciosMembresia=[];

  public RoleUser = "";

  public carritoMembresia = {
    items:[],
    total:0
  }

  public paciente:any;
  public allServicios:any [] = [];


  obtenerPacienteYMembresia(){
    const usuarioMembresia = new PacienteStorage();
    this.paciente = usuarioMembresia.verPacienteConMembresia();

    if( this.paciente == null || this.paciente.membresiaHabilitada == false ) {
      this.verDatos();
    }else{
      this.membresia = usuarioMembresia.verMembresiaPaciente();
      
      this.verDatos();
    }
  }

  public carrito = {
    totalSin: 0,
    totalCon: 0,
    items: []
  };


  public sede="";
  public nombre="LISTADO DE ";

  constructor(private _service: ServiciosService, private _router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.servicios = this.activatedRoute.snapshot.paramMap.get('servicio');
    this.nombre = this.nombre.concat(this.servicios.toUpperCase())
    this.getSede();
    this.obtenerRoleStorage();
    this.obtenerCarritoStorage();
    this.obtenerPacienteYMembresia();
    this.obtenerAllServicios();
  }



  getSede (){
    this.sede = localStorage.getItem('IdSede');
  }

  obtenerCarrito (){

    this.carrito = getCarritoStorage();

    if ( this.carrito == null ){
        this.carrito = {
          totalSin: 0,
          totalCon: 0,
          items: []
        };
    }

  }

  obtenerRoleStorage(){
    this.RoleUser = getDataStorage().role;
  }

  obtenerCarritoStorage(){
    const storageCarrito = new Carrito();
    this.carrito = storageCarrito.obtenerSotorageCarrito();
  }



  busquedaGeneral(event:any){
    if (event.target.value == '') {
      this.verDatos();
    } else if (event.target.value.length >= 3){
      this.serviceSi = filterServicios(this.allServicios, event.target.value)
    }
  }

  obtenerAllServicios(){
    let val = {
      idSede : this.sede
    }
    this._service.obtenerServiciosSede(val).subscribe((resp:any) =>{
      if(resp.ok){
        this.allServicios = resp['data']
      }
    })
  }

  verDatos(){
    this._service.obtenerServiciosSedeNombre(this.servicios, this.sede).subscribe(
      (res: any) => {
        this.verificarDatos(res['data']);
      },
    err => {
        console.log(<any>err);
    }
  );
  }

  verificarDatos(resp:any){
    //se itera en el HTML
    this.serviceSi = resp;
    // paginador 
    this.totalAmbulancia = resp.results;

  }

}
