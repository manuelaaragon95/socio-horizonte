import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BabyandMomComponent } from './babyand-mom.component';

describe('BabyandMomComponent', () => {
  let component: BabyandMomComponent;
  let fixture: ComponentFixture<BabyandMomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BabyandMomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BabyandMomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
