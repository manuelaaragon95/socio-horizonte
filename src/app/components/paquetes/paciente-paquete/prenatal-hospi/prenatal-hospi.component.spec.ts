import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrenatalHospiComponent } from './prenatal-hospi.component';

describe('PrenatalHospiComponent', () => {
  let component: PrenatalHospiComponent;
  let fixture: ComponentFixture<PrenatalHospiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrenatalHospiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrenatalHospiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
