import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AgregarGinecoObstetricosPorVisita } from '../../../interfaces/historia-clinica';
import Swal from 'sweetalert2'
import { HistoriaClinicaService } from 'src/app/services/historiaClinica/historia-clinica.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-gineco-por-visita',
  templateUrl: './gineco-por-visita.component.html',
  styleUrls: ['./gineco-por-visita.component.css']
})
export class GinecoPorVisitaComponent implements OnInit {

  @Input() _id='';
  @Input() agregarGinecoObstetricosPorVisita: AgregarGinecoObstetricosPorVisita = {} as AgregarGinecoObstetricosPorVisita;
  @Input() actualizar:boolean = false;
  
  constructor(private _HistoriaClinicaService: HistoriaClinicaService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  agregarGinecoObstetricos(form: NgForm){
    this.spinner.show();
    this.agregarGinecoObstetricosPorVisita = form.value
    this.agregarGinecoObstetricosPorVisita.idPaciente = this._id;
    this._HistoriaClinicaService.agregarGinecoObstetricosPorVisita ( this.agregarGinecoObstetricosPorVisita )
    .subscribe((data:any) => {
      if(data['ok']) {
        this.spinner.hide();
        Swal.fire({
          icon: 'success',
          title: '',
          text: 'Se guardaron los Antecedentes Gineco Obstetricos Historia por Visita',
        })
      }
    });
  }

  actualizarGinecoPorVisitalt(form: NgForm){
    this.spinner.show();
    let id = this.agregarGinecoObstetricosPorVisita._id;
    this.agregarGinecoObstetricosPorVisita = form.value;
    this.agregarGinecoObstetricosPorVisita.idPaciente = this._id;
    this.agregarGinecoObstetricosPorVisita._id = id
    this._HistoriaClinicaService.actualizarGinecoPorVisita(this.agregarGinecoObstetricosPorVisita._id, this.agregarGinecoObstetricosPorVisita)
    .subscribe((data:any) => {
      if(data['ok']){
        this.spinner.hide();
        Swal.fire({
          icon: 'success',
          title: '',
          text: 'SE ACTUALIZARON LOS ANTECEDENTES',
        })
      }
    })
  }

}
