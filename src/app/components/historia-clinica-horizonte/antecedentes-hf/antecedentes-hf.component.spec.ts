import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AntecedentesHFComponent } from './antecedentes-hf.component';

describe('AntecedentesHFComponent', () => {
  let component: AntecedentesHFComponent;
  let fixture: ComponentFixture<AntecedentesHFComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AntecedentesHFComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AntecedentesHFComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
