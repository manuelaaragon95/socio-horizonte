import { Component, Input, OnInit } from '@angular/core';
import { HistoriaClinicaService } from 'src/app/services/historiaClinica/historia-clinica.service';
import { Antecedentes, 
         AgregarGinecoObstetricosPorVisita, 
         MedicinaPreventivas, 
         EsquemaVacun , 
         nutricion, 
         antecedentesHF,
         antecedentesPNP,
         antecedentesPP,
         antecedentesGO} from '../../../interfaces/historia-clinica';

@Component({
  selector: 'app-historia-clinica-horizonte',
  templateUrl: './historia-clinica-horizonte.component.html',
  styleUrls: ['./historia-clinica-horizonte.component.css']
})
export class HistoriaClinicaHorizonteComponent implements OnInit {

  @Input() paciente={
    nombrePaciente:"",
    apellidoPaterno:"",
    apellidoMaterno: "",
    fechaNacimientoPaciente:"",
    curp:"",
    telefono:0,
    consultas: 0,
    _id:"",
    fechaRegistro:Date,
    genero:"",
    estadoPaciente:"",
    callePaciente:"",
    paisPaciente:"",
    cpPaciente:"",
    contactoEmergencia1Nombre:"",
    contactoEmergencia1Edad:"",
    contactoEmergencia1Telefono:"",
    correo: "",
    edad:0,
    paquetes : [],
    membresiaActiva:false,
    numeroExpediente:'',
    religion:'',
    tipoDeSangre:''
  };
  public pageTitle = "hojaEvo";
  public antecedentes:Antecedentes = {} as Antecedentes;
  public historialSginos :any[];
  public agregarGinecoObstetricosPorVisita: AgregarGinecoObstetricosPorVisita = {} as AgregarGinecoObstetricosPorVisita;
  public medicinaPreventiva: MedicinaPreventivas = {} as MedicinaPreventivas;
  public esquemaVacuncaion: EsquemaVacun = {} as EsquemaVacun;
  public nutricionNinos:nutricion = {} as nutricion;
  public antecedentesHeredoFamiliares:antecedentesHF = {} as antecedentesHF;
  public antecedentesPersonalesNoPatologicos:antecedentesPNP = {} as antecedentesPNP;
  public antecedentesPersonalesPatologicos:antecedentesPP = {} as antecedentesPP;
  public antecedentesGinecoObstetricosHistoria:antecedentesGO = {} as antecedentesGO;
  public actualizarAntecedentes:boolean = false;
  public actualizarGinecoPorVisitas:boolean = false;
  public actualizarMedicinaP:boolean = false;
  public actualizarVacunacionNinos:boolean = false;
  public actualizarNutricion:boolean = false;
  public actualizarHeredoFamiliaresXPaciente:boolean = false;
  public actualizarAntecedentesNoPatologicos:boolean = false;
  public actualizarAntecedentesPersonalesPatologicos:boolean = false;
  public actualizarGinecoHistoria:boolean = false;

  constructor(private _HistoriaClinicaService: HistoriaClinicaService) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.obtenerAntecedentes(this.paciente._id);
    }, 1000);
  }

  obtenerAntecedentes(id){
    this.antecedentes.idPaciente= id;
    this._HistoriaClinicaService.obtenerAntecedentes( this.antecedentes.idPaciente).subscribe((resp:any) => {
      if (Array.isArray(resp['data']) && resp['data'].length > 0) {
        this.actualizarAntecedentes = true;
        this.antecedentes = resp['data'][0];
      }
    })
  }

  verSignosVitalesAnteriores(){
    this._HistoriaClinicaService.obtenerHistroialSignosVitalesPaciente(this.paciente._id)
    .subscribe( (resp:any) =>   {
      this.historialSginos = resp['data'];
    });
  }

  obtenerGinecoPorVisitas() {
    this._HistoriaClinicaService.obtenerGinecoPorVisita(this.paciente._id)
    .subscribe((resp:any) => {
      if (Array.isArray(resp['data']) && resp['data'].length > 0 ) {
        this.actualizarGinecoPorVisitas = true;
        this.agregarGinecoObstetricosPorVisita = resp['data'][0];
      }
    })
  }

  obtenerMedicinaP(){
    this._HistoriaClinicaService.obtenerMedicinaPreventiva(this.paciente._id)
    .subscribe((resp:any) => {
      if (Array.isArray(resp['data']) && resp['data'].length > 0) {
        this.actualizarMedicinaP = true;
        this.medicinaPreventiva = resp['data'][0];
      }
    })
  }

  obtenerVacunacionNinos() {
    this.esquemaVacuncaion.idPaciente = this.paciente._id;
    this._HistoriaClinicaService.obtenerEV(this.esquemaVacuncaion.idPaciente)
    .subscribe((resp:any) => {
      if(Array.isArray(resp['data']) && resp['data'].length > 0 ) {
        this.actualizarVacunacionNinos = true;
        this.esquemaVacuncaion = resp['data'][0];
      }
    })
  }

  obtenerNutricion() {
    this.nutricionNinos.idPaciente=this.paciente._id;
    this._HistoriaClinicaService.obtenerNutricion(this.nutricionNinos.idPaciente)
    .subscribe((resp:any) => {
      if(Array.isArray(resp['data']) && resp['data'].length > 0) {
        this.actualizarNutricion = true
        this.nutricionNinos = resp['data'][0]
      }       
    })
  }

  obtenerHeredoFamiliaresXPaciente(){
    this._HistoriaClinicaService.obtenerHeredoFamiliares(this.paciente._id)
    .subscribe((resp:any) => {
      if (Array.isArray(resp['data']) && resp['data'].length > 0) {
        this.actualizarHeredoFamiliaresXPaciente = true;
        this.antecedentesHeredoFamiliares = resp['data'][0];
      }
    })
  }

  verAntecedentesNoPatologicos(){
    this._HistoriaClinicaService.verAntecendetesNoPatoloogicos(this.paciente._id)
    .subscribe((resp) => {
      if(Array.isArray(resp['data']) && resp['data'].length > 0) {
        this.actualizarAntecedentesNoPatologicos = true;
        this.antecedentesPersonalesNoPatologicos = resp['data'][0]
      }
    });
  }

  verAntecedentesPersonalesPatologicos(){
    this.antecedentesPersonalesPatologicos.idPaciente = this.paciente._id;
    this._HistoriaClinicaService.obtenerAntecedentePersonalePatologicos(this.paciente._id)
    .subscribe((resp) => { 
      if(Array.isArray(resp['data']) && resp['data'].length > 0) {
        this.actualizarAntecedentesPersonalesPatologicos = true;
       this.antecedentesPersonalesPatologicos = resp['data'][0];
      }
    });
  } 

  obtenerGinecoHistoria(){
    this._HistoriaClinicaService.obtenerGinecoHistoria(this.paciente._id)
    .subscribe((resp:any) => {
      if(Array.isArray(resp['data']) && resp['data'].length > 0){
        this.actualizarGinecoHistoria = true;
        this.antecedentesGinecoObstetricosHistoria = resp['data'][0];
      }
    })
  }
}
