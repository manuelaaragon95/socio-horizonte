import { Component, Input, OnInit } from '@angular/core';
import { antecedentesPP } from '../../../interfaces/historia-clinica';
import Swal from 'sweetalert2'
import { HistoriaClinicaService } from 'src/app/services/historiaClinica/historia-clinica.service';
import { NgForm } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-antecedentes-pp',
  templateUrl: './antecedentes-pp.component.html',
  styleUrls: ['./antecedentes-pp.component.css']
})
export class AntecedentesPpComponent implements OnInit {

  @Input() _id='';
  @Input() antecedentesPersonalesPatologicos:antecedentesPP = {} as antecedentesPP;
  @Input() actualizar:boolean = false;

  constructor(private _HistoriaClinicaService: HistoriaClinicaService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  agregarPersonalesPatologicos(form: NgForm){
    this.spinner.show();
    this.antecedentesPersonalesPatologicos = form.value;
    this.antecedentesPersonalesPatologicos.idPaciente = this._id;
    this._HistoriaClinicaService.agregarPersonalesPatologicos(this.antecedentesPersonalesPatologicos)
    .subscribe( ( data:any ) => {
      if(data['ok']){
        this.spinner.hide();
        Swal.fire({
          icon: 'success',
          title: '',
          text: 'SE GUARDO LA HISTORIA CLINICA',
        })
      }
    });
  }

  actualizarPersonalesPatologicos(form: NgForm){
    this.spinner.show();
    let id = this.antecedentesPersonalesPatologicos._id;
    this.antecedentesPersonalesPatologicos = form.value;
    this.antecedentesPersonalesPatologicos._id = id;
    this.antecedentesPersonalesPatologicos.idPaciente = this._id;
    this._HistoriaClinicaService.actualizarPersonalesPatologicos(this.antecedentesPersonalesPatologicos._id, this.antecedentesPersonalesPatologicos)
    .subscribe((data:any) => {
      if(data['ok']) {
        this.spinner.hide();
        Swal.fire({
          icon: 'success',
          title: '',
          text: 'SE ACTUALIZO LA HISTORIA CLINICA',
        })
      }
    })
  }

}
