import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AntecedentesGinecoObsComponent } from './antecedentes-gineco-obs.component';

describe('AntecedentesGinecoObsComponent', () => {
  let component: AntecedentesGinecoObsComponent;
  let fixture: ComponentFixture<AntecedentesGinecoObsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AntecedentesGinecoObsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AntecedentesGinecoObsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
