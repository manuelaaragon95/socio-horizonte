import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicinaPrevComponent } from './medicina-prev.component';

describe('MedicinaPrevComponent', () => {
  let component: MedicinaPrevComponent;
  let fixture: ComponentFixture<MedicinaPrevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicinaPrevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicinaPrevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
