import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogacionPAYSComponent } from './interrogacion-pays.component';

describe('InterrogacionPAYSComponent', () => {
  let component: InterrogacionPAYSComponent;
  let fixture: ComponentFixture<InterrogacionPAYSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterrogacionPAYSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogacionPAYSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
