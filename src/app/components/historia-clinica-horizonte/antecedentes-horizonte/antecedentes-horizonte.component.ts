import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HistoriaClinicaService } from 'src/app/services/historiaClinica/historia-clinica.service';
import Swal from 'sweetalert2'
import { Antecedentes } from '../../../interfaces/historia-clinica';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-antecedentes-horizonte',
  templateUrl: './antecedentes-horizonte.component.html',
  styleUrls: ['./antecedentes-horizonte.component.css']
})
export class AntecedentesHorizonteComponent implements OnInit {

  @Input() _id='';
  @Input() edad;
  @Input() antecedentes:Antecedentes = {} as Antecedentes;
  @Input() actualizar:boolean = false;
  
  constructor(private _HistoriaClinicaService: HistoriaClinicaService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  agregarAntecedentes(form: NgForm){
    this.spinner.show();
    this.antecedentes= form.value
    this.antecedentes.idPaciente = this._id;
    this._HistoriaClinicaService.agregarHistoriaClinica(this.antecedentes)
    .subscribe(  (data:any) => { 
        if(data['ok']){
          this.spinner.hide();
           Swal.fire({
            icon: 'success',
            title: '',
            text: 'SE AGREGARON LOS ANTECEDENTES',
          })
        }
    });
  }

  actualizarAntecedentes(form: NgForm){
    this.spinner.show();
    let id = this.antecedentes._id;
    this.antecedentes = form.value;
    this.antecedentes.idPaciente = this._id;
    this.antecedentes._id = id;
    this._HistoriaClinicaService.actualizarAntecedentesPaciente(this.antecedentes._id, this.antecedentes)
    .subscribe((data:any) => {
      if(data['ok']){ 
        this.spinner.hide();
        Swal.fire({
          icon: 'success',
          title: '',
          text: 'SE ACTUALIZARON LOS ANTECEDENTES',
        })
      }
    })
  }

}
