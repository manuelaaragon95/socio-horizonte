import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AntecedentesHorizonteComponent } from './antecedentes-horizonte.component';

describe('AntecedentesHorizonteComponent', () => {
  let component: AntecedentesHorizonteComponent;
  let fixture: ComponentFixture<AntecedentesHorizonteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AntecedentesHorizonteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AntecedentesHorizonteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
