import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMedicosComponent } from './select-medicos.component';

describe('SelectMedicosComponent', () => {
  let component: SelectMedicosComponent;
  let fixture: ComponentFixture<SelectMedicosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectMedicosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
