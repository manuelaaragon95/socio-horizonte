import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBarSocioHorizonteComponent } from './nav-bar-socio-horizonte.component';

describe('NavBarSocioHorizonteComponent', () => {
  let component: NavBarSocioHorizonteComponent;
  let fixture: ComponentFixture<NavBarSocioHorizonteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavBarSocioHorizonteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarSocioHorizonteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
