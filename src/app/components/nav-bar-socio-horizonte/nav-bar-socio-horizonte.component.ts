import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Personal } from 'src/app/interfaces/personal';
import { ModulesService } from 'src/app/services/modules/modules.service';

@Component({
  selector: 'app-nav-bar-socio-horizonte',
  templateUrl: './nav-bar-socio-horizonte.component.html',
  styleUrls: ['./nav-bar-socio-horizonte.component.css']
})
export class NavBarSocioHorizonteComponent implements OnInit {
  public pagina = 0;
  public totalAmbulancia: string;
  public usuario: Personal;
  public modules:any[]=[];

  constructor(public router: Router,
              public modulesService: ModulesService){}

  ngOnInit(): void {
    this.getRoleLocalStorage();
  }

  getRoleLocalStorage(){
    // SETEA LOS DATOS DEL USURAIO EN EL LOCALSTORAGE
    this.usuario = JSON.parse(localStorage.getItem('usuario'));
    if( this.usuario == null || this.usuario == undefined){
      return;
    }else{
      this.getModuleByRole();
    }
  }

  getModuleByRole(){
    this.modulesService.getModules( this.usuario._id ).subscribe( (data:any)  => {
      //  OBTENEMOS TODOS LOS MODULOS DEL USUARIO
      this.modules = data['data'];
      this.totalAmbulancia = data.data.usuario;
    })
  }
}
