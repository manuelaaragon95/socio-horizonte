// Sesison methods 
export const getUserNameSession = () => {
    const usuario = JSON.parse(localStorage.getItem('usuario')).nombre;
    return usuario
};

export const getUserIdSession = () => {
    const usuario = JSON.parse(localStorage.getItem('usuario'))._id;
    return usuario
    
};

// filters searchs

export const filterNameByPaciente = (pacientes = [], name) => {

    //recibe el array de los pacientes
    // recibe el parametro de busqueda

    const pacientesFilter = [];

    pacientes.forEach( (pacientes:any) => {
        
        var  nombrePaciente = ""
        var   apellidoPaterno = ""
        var    apellidoMaterno = ""

        if( pacientes.idPaciente == undefined ) {

            //validacion para los populates de la data
            nombrePaciente = pacientes.nombrePaciente
            apellidoPaterno = pacientes.apellidoPaterno
            apellidoMaterno  = pacientes.apellidoMaterno

        }else {

            // si la data del paciente no trae la data dentro del populate 
             nombrePaciente = pacientes.idPaciente.nombrePaciente
             apellidoPaterno = pacientes.idPaciente.apellidoPaterno
             apellidoMaterno  = pacientes.idPaciente.apellidoMaterno
        }

        nombrePaciente.trim() 
        apellidoPaterno.trim()
        // ponemos el trim de los nombres

        nombrePaciente.toUpperCase(); 
        apellidoPaterno.toUpperCase();

        // concatenamos los apellidos  al nombre
        nombrePaciente += " "+apellidoPaterno         
        nombrePaciente += " " +apellidoMaterno;


        if(nombrePaciente.includes(name.toUpperCase())){
            pacientesFilter.push( pacientes );
            //agregamos al array los pacientes
        }

    });

    //retorna los pacientes encontrados en el array
    return pacientesFilter;
}

// funcion que calcula las fechas de nacimiento de acuerdo al curp
export const calcAge =  ( curp = "" ) => {
    //en el curp es anio/mes/dia
  const agesCutted  =  curp.slice( 4, -8 );  
  
  let year = agesCutted.slice(0,2 )
  let month = agesCutted.slice(2,4 )
  let day = agesCutted.slice(4,6 )
   year  = "19" + year;
   const newDate =  year+"-"+month+"-"+day;
   return newDate
};


export const validateAnticipoLess = ( montos, anticipo ) =>  {
    
    const metodoPago = {
        metodo:"",
        valid: true
    }

    const { 
        montoEfectivo,
        montoTarjetaDebito,
        montoTarjteCredito,
        montoTranferencia,
        efectivo,
        transferencia,
        tarjetCredito,
        tarjetaDebito

    } = montos;
    // enviamos la infor venta y hacemos el destructuring

    
    if( efectivo == "true" ) {
        if(  montoEfectivo  >=   anticipo ){
        
            // comparamos los metodos de pago con el anticipo
            
            metodoPago.metodo = "Efectivo"
            return metodoPago 
        
        }
    }else if( tarjetCredito == 'true' ){

        if(  montoTarjteCredito  >=   anticipo ){
        
            // comparamos los metodos de pago con el anticipo
            
            metodoPago.metodo = "TarjetaCredito"
            return metodoPago 
        
        }
    }else if( tarjetaDebito == 'true' ) {

        if(  montoTarjetaDebito  >=   anticipo ){
        
            // comparamos los metodos de pago con el anticipo
            
            metodoPago.metodo = "TarjetaDebito"
            return metodoPago 
        
        }

    }else if( transferencia == 'true' ) { 

        if(  montoTranferencia  >=   anticipo ){
        
            // comparamos los metodos de pago con el anticipo
            
            metodoPago.metodo = "transferencia"
            return metodoPago 
        
        }
    }
    return false;
};