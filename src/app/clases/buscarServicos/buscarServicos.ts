export const filterServicios = (servicios = [], nombre) => {
    const servicosFilter = [];
    servicios.forEach((element:any)=>{
        if(element.idServicio != null){
            let est = element.idServicio.ESTUDIO.toLowerCase();
            if (est.includes(nombre.toLowerCase())) {
                servicosFilter.push(element)
            }  
        }
    })

    servicosFilter.sort(function(a, b){
        if(a.idServicio.ESTUDIO < b.idServicio.ESTUDIO) return -1; if(a.idServicio.ESTUDIO > b.idServicio.ESTUDIO) return 1; return 0;
    })

    return servicosFilter;
}

export const filterResultados = (pacientes = [], buscar) => {
    const pacientesFilter = [];

    pacientes.forEach((element:any)=>{
        let nombre = element.idPaciente.nombrePaciente.trim();
        let apellidoP = element.idPaciente.apellidoPaterno.trim();
        let apellidoM = element.idPaciente.apellidoMaterno.trim();
        let nombreCompleto = ((nombre.concat(' ',apellidoP,' ', apellidoM)).normalize("NFD").replace(/[\u0300-\u036f]/g, "")).toLowerCase();
        //console.log(nombreCompleto);
        if (nombreCompleto.includes(buscar.toLowerCase())) {
            pacientesFilter.push(element)
        }  
    })
    return pacientesFilter;
}