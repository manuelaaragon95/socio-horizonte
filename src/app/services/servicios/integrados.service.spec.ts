import { TestBed } from '@angular/core/testing';

import { IntegradosService } from './integrados.service';

describe('IntegradosService', () => {
  let service: IntegradosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IntegradosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
