import { TestBed } from '@angular/core/testing';

import { PedidosSedesService } from './pedidos-sedes.service';

describe('PedidosSedesService', () => {
  let service: PedidosSedesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PedidosSedesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
