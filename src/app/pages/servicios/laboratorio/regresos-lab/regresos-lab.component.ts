import { Component, OnInit } from '@angular/core';
import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import jsPDF from 'jspdf';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServiciosService } from 'src/app/services/serviciosInt/servicios.service';
import { filterResultados } from 'src/app/clases/buscarServicos/buscarServicos';

@Component({
  selector: 'app-regresos-lab',
  templateUrl: './regresos-lab.component.html',
  styleUrls: ['./regresos-lab.component.css']
})
export class RegresosLabComponent implements OnInit {

  public totalpaciente:string;
  public pagina = 0;
  public consultas:any=[];
  noSe: any;
  public servicio='';
  public pedidosLab = [];
  public sede = ''

  constructor(public _consultaService: ConsultaService,
              public _consultaPaciente: PacientesService,
              private activatedRoute: ActivatedRoute,
              private spinner: NgxSpinnerService,
              private _serviciosService: ServiciosService) {}

  ngOnInit(): void {
    this.servicio = this.activatedRoute.snapshot.paramMap.get('servicio');
    this.sede = localStorage.getItem('cede')
    this.getBitacoraLabs();
  }

  obtenerCosnultaLab(){
    /* this._consultaService.verListaLaboratorio(this.servicio).subscribe((resp:any)=>{
      console.log(resp);
      
    }) */
    /* this._consultaService.verListaLaboratorio().subscribe( (data) =>   {
        this.consultas = data['data'].reverse();
        this.totalpaciente = data['data'].results;
    }); */
  }

  getBitacoraLabs() {
    this.spinner.show()
    this._serviciosService.getBitacoraLabs('LABORATORIO',this.sede)
    .subscribe(
      (data) => {
        /* this.pedidosLab = data['data'].reverse() */
        this.pedidosLab = this.setRegresos(data['data'])
        this.totalpaciente = data['data'].results
        this.spinner.hide();
      }
    )
  }

  setRegresos(laboratorios:any){
    laboratorios.forEach(element => {
      if (element.idPaciente != null) {
        this.pedidosLab.push(element)
      }
    });
    var found ={};
    var resp = this.pedidosLab.filter(function(element){
      return found.hasOwnProperty(element.idPaciente._id)? false : (found[element.idPaciente._id]=true);
    });
    return resp.reverse();
  }

  busquedaGeneral(event:any){
    if (event.target.value == '') {
      this.getBitacoraLabs();
    } else if (event.target.value.length >= 3){
      this.pedidosLab = filterResultados(this.pedidosLab, event.target.value)
    }
  }

  imp(){
    let values: any;
    values = this.noSe.map((elemento) => Object.values(elemento));
    const doc:any = new jsPDF();
    doc.text(12, 9, "BITÁCORA DE RAYOS X");
    doc.autoTable({
      head: [['#', 'Fecha', 'Nombre', 'Edad', 'Sexo', 'Sede', 'Estudio']],
      body: values
    })
    doc.save('Bitácora_De_Rayos_X.pdf')
  }
 
}
