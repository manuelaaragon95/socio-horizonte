import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PacientesEstudiosLabComponent } from './pacientes-estudios-lab.component';

describe('PacientesEstudiosLabComponent', () => {
  let component: PacientesEstudiosLabComponent;
  let fixture: ComponentFixture<PacientesEstudiosLabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PacientesEstudiosLabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PacientesEstudiosLabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
