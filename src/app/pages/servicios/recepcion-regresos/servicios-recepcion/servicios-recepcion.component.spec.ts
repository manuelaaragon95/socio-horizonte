import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiciosRecepcionComponent } from './servicios-recepcion.component';

describe('ServiciosRecepcionComponent', () => {
  let component: ServiciosRecepcionComponent;
  let fixture: ComponentFixture<ServiciosRecepcionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiciosRecepcionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiciosRecepcionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
