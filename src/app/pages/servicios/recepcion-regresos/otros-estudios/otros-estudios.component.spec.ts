import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtrosEstudiosComponent } from './otros-estudios.component';

describe('OtrosEstudiosComponent', () => {
  let component: OtrosEstudiosComponent;
  let fixture: ComponentFixture<OtrosEstudiosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtrosEstudiosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtrosEstudiosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
