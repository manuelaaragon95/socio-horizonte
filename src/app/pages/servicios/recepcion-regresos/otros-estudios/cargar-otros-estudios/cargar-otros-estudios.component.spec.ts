import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CargarOtrosEstudiosComponent } from './cargar-otros-estudios.component';

describe('CargarOtrosEstudiosComponent', () => {
  let component: CargarOtrosEstudiosComponent;
  let fixture: ComponentFixture<CargarOtrosEstudiosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CargarOtrosEstudiosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CargarOtrosEstudiosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
