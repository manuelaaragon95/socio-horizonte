import { Component, OnInit } from '@angular/core';
import { ServiciosService } from 'src/app/services/serviciosInt/servicios.service';
import { USGService } from 'src/app/services/usg/usg.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-bitacora-ultrasonido',
  templateUrl: './bitacora-ultrasonido.component.html',
  styleUrls: ['./bitacora-ultrasonido.component.css']
})
export class BitacoraUltrasonidoComponent implements OnInit {

  public consultas:any=[]
  public pagina = 0;
  public totalpaciente:string;
  public sede = ''

  constructor(  public _usgservicio : USGService,
    private _servicios: ServiciosService,
    private spinner : NgxSpinnerService) { }

  ngOnInit(): void {
    this.sede = localStorage.getItem('cede')
    this.obtenerCosnultaUltra()
  }

  obtenerCosnultaUltra(){
    this.spinner.show()
    this._servicios.getBitacoraLabs('ultrasonido',this.sede)
    .subscribe((data:any) => {
      if( data.ok ) {
        this.consultas = this.setRegresos( data['data'] )
        this.spinner.hide();
      }
    })
  }

  setRegresos(ultrasonidos:any){
    ultrasonidos.forEach(element => {
      if (element.idPaciente != null) {
        this.consultas.push(element)
      }
    });
    var found ={};
    var resp = this.consultas.filter(function(element){
      return found.hasOwnProperty(element.idPaciente._id)? false : (found[element.idPaciente._id]=true);
    });
    return resp.reverse();
  }

  setDataUltras(data:any) {
    this.consultas = data.reverse();
  }
}
