import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServiciosService } from 'src/app/services/serviciosInt/servicios.service';

@Component({
  selector: 'app-bitacora-rayos-x',
  templateUrl: './bitacora-rayos-x.component.html',
  styleUrls: ['./bitacora-rayos-x.component.css']
})
export class BitacoraRayosXComponent implements OnInit {
  public consultas:any=[]
  public totalpaciente:string;
  public pagina = 0;
  public sede = '';
  public pedidosUSG = []

  constructor(private spinner : NgxSpinnerService,
                private _servicios: ServiciosService) { }

  ngOnInit(): void {
    this.sede = localStorage.getItem('cede')
    this.spinner.show()
    this.obtenerCosnultaUltra()
    // this.getBitacoraUSG()
  }
  
  obtenerCosnultaUltra(){
    this._servicios.getBitacoraLabs('XRAY',this.sede)
    .subscribe((data:any) => {
      if( data.ok ) {
        this.setDataUltras( data.data )
        this.spinner.hide();
      }
    })
  }

  setDataUltras(data:any) {
    let array:any[] = [];
    data.forEach(element => {
      if(element.idPaciente != null){
        array.push(element)
      }
    });
    this.consultas = array.reverse();
  }
}
