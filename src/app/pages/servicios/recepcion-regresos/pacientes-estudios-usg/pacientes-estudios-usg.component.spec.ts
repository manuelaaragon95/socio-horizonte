import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PacientesEstudiosUsgComponent } from './pacientes-estudios-usg.component';

describe('PacientesEstudiosUsgComponent', () => {
  let component: PacientesEstudiosUsgComponent;
  let fixture: ComponentFixture<PacientesEstudiosUsgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PacientesEstudiosUsgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PacientesEstudiosUsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
