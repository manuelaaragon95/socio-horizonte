import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServiciosService } from 'src/app/services/serviciosInt/servicios.service';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-pacientes-estudios-usg',
  templateUrl: './pacientes-estudios-usg.component.html',
  styleUrls: ['./pacientes-estudios-usg.component.css']
})
export class PacientesEstudiosUsgComponent implements OnInit {

  public id='';
  public pedidosUsg:any[]=[]
  constructor(private _route: ActivatedRoute,
              private _serviciosService: ServiciosService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.id = this._route.snapshot.paramMap.get('id');
    this.obtenerRegresos();
  }

  obtenerRegresos(){
    this.spinner.show();
    this._serviciosService.obtenerRegresosHistoriaClinica(this.id,'ultrasonido').subscribe((resp:any)=>{
      if(resp.ok){
        this.pedidosUsg = resp['data'].reverse()
        this.spinner.hide()
      }
    })
  }

  obtenerVAlores(item){
    this.spinner.show();
    this._serviciosService.getResultadoPorEstudio(item._id).subscribe((resp:any)=>{
      if (resp.ok) {
        this.imprimir(resp);
      }
    })
  }

  imprimir(data:any){
    let imgHeader = '../../../../../assets/images/socio_horizonte.png';
    var marca = '../../../../../assets/images/favicon 2.png';
    var CUADROS = '../../../../../assets/images/linea 2.png';
    var piePagina = '../../../../../assets/images/piePaginaTlaya.png'

    const patient = data['data']['paciente'];
    const NOMBRE_ESTUDIO = data['data']['servicio']['ESTUDIO'];
    const OBSERVACIONES =data['data']['resultados'][0].obtenidos.observaciones
    const IMGS =data['data']['resultados'][0].obtenidos.imgs
    const DIAGNOSTICOS =data['data']['resultados'][0].obtenidos.diagnostico
    const INTERPRETACION =data['data']['resultados'][0].obtenidos.interpretacion
    const SOLICITO = data['data']['solicito'];
    const qRCode = data['data']['imgQrCode']
    const FECHAPEDIDO = data['data']['fechaPedido'];;
    const fechaSplited = FECHAPEDIDO.split('T')[0];

    const doc:any = new jsPDF( {
      unit: "cm",
      format: 'a4',
    });
    
    doc.addImage(imgHeader, 'PNG', 1, 1, 5, 2);
    doc.addImage(CUADROS, 'PNG', 12, 1.8, 10, 1);
    doc.addImage(qRCode, 'PNG', 10, 1, 2.5, 2.5);
    doc.addImage(marca, 'PNG', 1, 6, 19, 25);
    doc.setTextColor(0, 0, 0);
    doc.setFontSize(10);
    doc.setFont('helvetica')
    doc.text('PACIENTE:', 1, 4)
    doc.text(patient.nombrePaciente + ' ' + patient.apellidoPaterno + ' ' + patient.apellidoMaterno, 3, 4)
    doc.text('EDAD: ', 9, 4);
    doc.text(patient.edad + ' AÑOS', 10.5, 4);
    doc.text('GÉNERO: ', 13.5, 4);
    doc.text(patient.genero, 16, 4);
    doc.text('FECHA DE NACIMIENTO: ', 1, 4.5 );
    const nacimiento = String(patient.fechaNacimientoPaciente)
    
    if (nacimiento == 'undefined' || nacimiento == '') {
      doc.text('-- / --- / ----', 5.5, 4.5);
    }else {
      let fechaNacimiento;
      fechaNacimiento = nacimiento.slice(0, -14)
      doc.text(fechaNacimiento, 5.5, 4.5);
    }
    doc.text('FECHA EN QUE SE REALIZÓ: ', 1, 5 );
    doc.text( fechaSplited , 6.2, 5 );
    doc.text('SOLICITÓ: ',  13.5, 4.5);
    doc.text( SOLICITO , 15.5, 4.5);
    doc.text( "Dra. C. Perez Toral", 15, 27.2 )
    doc.text( "CMU-CU266", 15, 27.5 )
    doc.setFontSize(8)
    doc.addImage(piePagina, 1, 27.5, 18, 1)
    doc.setFontSize(14);
    
    if(  NOMBRE_ESTUDIO.includes('(')){
      const newCharacter = NOMBRE_ESTUDIO.split('(');
      newCharacter[1] = "("+ newCharacter[1];
      doc.text(newCharacter[0], 6, 6)
      doc.text(newCharacter[1], 3, 6.5);
      doc.text(newCharacter[0], 6, 6)
      doc.text(newCharacter[1], 3, 6.5);
      doc.text(newCharacter[0], 6, 6)
      doc.text(newCharacter[1], 3, 6.5);
      doc.text(newCharacter[0], 6, 6)
      doc.text(newCharacter[1], 3, 6.5);
    } else if(NOMBRE_ESTUDIO.length>= 35 ){ 
      doc.text(NOMBRE_ESTUDIO, 3.5, 6)
      doc.text(NOMBRE_ESTUDIO, 3.5, 6)
      doc.text(NOMBRE_ESTUDIO, 3.5, 6)
      doc.text(NOMBRE_ESTUDIO, 3.5, 6)
      doc.text(NOMBRE_ESTUDIO, 3.5, 6)
    } else {
      doc.text(NOMBRE_ESTUDIO, 1, 6)
      doc.text(NOMBRE_ESTUDIO, 1, 6)
      doc.text(NOMBRE_ESTUDIO, 1, 6)
      doc.text(NOMBRE_ESTUDIO, 1, 6)
      doc.text(NOMBRE_ESTUDIO, 1, 6)
    }
    
    doc.text( "INTERPRETACIÓN:", 1, 8 )
    doc.text( INTERPRETACION, 3, 9 )
    doc.text("DIAGNOSTICO:", 1, 19 )
    doc.text(DIAGNOSTICOS, 3, 19.8 )
    doc.text('OBSERVACIONES:', 1, 24)
    doc.text(OBSERVACIONES, 3, 24.5)
    
    if( IMGS.length > 0 ) {
      doc.addPage();
      doc.addImage(imgHeader, 'PNG', 1, 1, 5, 2);
      doc.addImage(CUADROS, 'PNG', 12, 1.8, 10, 1);
      doc.addImage(marca, 'PNG', 1, 6, 19, 25);
      doc.addImage(qRCode, 'PNG', 10, 1, 2.5, 2.5);
      doc.setTextColor(0, 0, 0);
      doc.setFontSize(10);
      doc.setFont('helvetica')
      doc.text('PACIENTE:', 1, 4)
      doc.text(patient.nombrePaciente + ' ' + patient.apellidoPaterno + ' ' + patient.apellidoMaterno, 3, 4)
      doc.text('EDAD: ', 9,4);
      //SETEAT EDAD
      doc.text(patient.edad + ' AÑOS', 10.5, 4);
      doc.text('GÉNERO: ', 13, 4);
      doc.text(patient.genero, 15, 4);
      doc.text('FECHA DE NACIMIENTO: ', 1, 4.4);
      doc.text('FECHA EN QUE SE REALIZÓ: ', 1, 5 );
      doc.text( fechaSplited , 6.2, 5 );
      doc.setFontSize(8)
      doc.text( "Dra. C. Perez Toral", 15, 27.2 )
      doc.text( "CMU-CU266", 15, 27.5 )
      doc.addImage(piePagina, 1, 27.5, 18, 1)
      // Setear Fecha paciente
      const nacimiento = String(patient.fechaNacimientoPaciente)
      if (nacimiento == 'undefined' || nacimiento == '') {
        doc.text('-- / --- / ----', 5.5, 4.4);
      }else {
        let fechaNacimiento;
        fechaNacimiento = nacimiento.slice(0, -14)
        doc.text(fechaNacimiento, 5.5, 4.4);
      }
      
      doc.text('SOLICITÓ: ',  13, 4.4);
      doc.text( SOLICITO , 15, 4.4);
      doc.setFontSize(14);
      
      if(  NOMBRE_ESTUDIO.includes('(')){
        const newCharacter = NOMBRE_ESTUDIO.split('(');
        newCharacter[1] = "("+ newCharacter[1];
        doc.text(newCharacter[0], 6, 6);
        doc.text(newCharacter[1], 3, 6.5);
        doc.text(newCharacter[0], 6, 6)
        doc.text(newCharacter[1], 3, 6.5);
        doc.text(newCharacter[0], 6, 6)
        doc.text(newCharacter[1], 3, 6.5);
        doc.text(newCharacter[0], 6, 6)
        doc.text(newCharacter[1], 3, 6.5);
      } else if(NOMBRE_ESTUDIO.length>= 35 ){ 
        doc.text(NOMBRE_ESTUDIO, 3.5, 6)
        doc.text(NOMBRE_ESTUDIO, 3.5, 6)
        doc.text(NOMBRE_ESTUDIO, 3.5, 6)
        doc.text(NOMBRE_ESTUDIO, 3.5, 6)
        doc.text(NOMBRE_ESTUDIO, 3.5, 6)
      }else {
        doc.text(NOMBRE_ESTUDIO, 1, 6)
        doc.text(NOMBRE_ESTUDIO, 1, 6)
        doc.text(NOMBRE_ESTUDIO, 1, 6)
        doc.text(NOMBRE_ESTUDIO, 1, 6)
        doc.text(NOMBRE_ESTUDIO, 1, 6)
      }
      
      doc.text("IMAGENES: ", 1, 7);
      var countX = 2 , countY = 7.3;
      
      for(let index = 0; index < IMGS.length; index++) {
        if( index == 6 || index == 12 || index == 18 || index == 24){
          countX = 2, countY = 7.3;
          doc.addPage();
          doc.addImage(imgHeader, 'PNG', 1.5, 1, 4, 2);
          // doc.text(REALIZO, 9, 28)
          doc.addImage(CUADROS, 'PNG', 19, 1, 1, 1);
          doc.addImage(marca, 'PNG', 1, 6, 19, 25);
          doc.addImage(qRCode, 'PNG', 10, 1, 2.5, 2.5);
          doc.setTextColor(0, 0, 0);
          doc.setFontSize(10);
          doc.setFont('helvetica')
          doc.text('PACIENTE:', 1, 4)
          doc.text(patient.nombrePaciente + ' ' + patient.apellidoPaterno + ' ' + patient.apellidoMaterno, 3, 4)
          doc.text('EDAD: ', 9,4);
          //SETEAT EDAD
          doc.text(patient.edad + ' AÑOS', 10.5, 4);
          doc.text('GÉNERO: ', 13, 4);
          doc.text(patient.genero, 15, 4);
          doc.text('FECHA DE NACIMIENTO: ', 1, 4.4);
          doc.text('FECHA EN QUE SE REALIZÓ: ', 1, 5 );
          doc.text( fechaSplited , 6.2, 5 );
          // doc.text( "Dra. C. Perez Toral", 15, 28 )
          // doc.text( "CMU-CU266", 15, 28.5)
          doc.setFontSize(8)
          doc.text( "Dra. C. Perez Toral", 15, 27.2 )
          doc.text( "CMU-CU266", 15, 27.5 )
          doc.addImage(piePagina, 1, 27.5, 18, 1)
          // Setear Fecha paciente
          const nacimiento = String(patient.fechaNacimientoPaciente)
          if (nacimiento == 'undefined' || nacimiento == '') {
            doc.text('-- / --- / ----', 5.5, 4.4);
          }else {
            let fechaNacimiento;
            fechaNacimiento = nacimiento.slice(0, -14)
            doc.text(fechaNacimiento, 5.5, 4.4);
          }
          
          doc.text('SOLICITÓ: ',  13, 4.4);
          doc.text( SOLICITO , 15, 4.4);
          
          if(  NOMBRE_ESTUDIO.includes('(')){
            doc.setFontSize(14);
            const newCharacter = NOMBRE_ESTUDIO.split('(');
            newCharacter[1] = "("+ newCharacter[1];
            doc.text(newCharacter[0], 6, 6);
            doc.text(newCharacter[1], 3, 6.5);
            doc.text(newCharacter[0], 6, 6)
            doc.text(newCharacter[1], 3, 6.5);
            doc.text(newCharacter[0], 6, 6)
            doc.text(newCharacter[1], 3, 6.5);
            doc.text(newCharacter[0], 6, 6)
            doc.text(newCharacter[1], 3, 6.5);
          }else if(NOMBRE_ESTUDIO.length>= 35 ){ 
            doc.setFontSize(14);
            doc.text(NOMBRE_ESTUDIO, 3.5, 6)
            doc.text(NOMBRE_ESTUDIO, 3.5, 6)
            doc.text(NOMBRE_ESTUDIO, 3.5, 6)
            doc.text(NOMBRE_ESTUDIO, 3.5, 6)
            doc.text(NOMBRE_ESTUDIO, 3.5, 6)
          }else {
            doc.setFontSize(14);
            doc.text(NOMBRE_ESTUDIO, 1, 6)
            doc.text(NOMBRE_ESTUDIO, 1, 6)
            doc.text(NOMBRE_ESTUDIO, 1, 6)
            doc.text(NOMBRE_ESTUDIO, 1, 6)
            doc.text(NOMBRE_ESTUDIO, 1, 6)
          }
          
          doc.text("IMAGENES: ", 1, 7);
        }
        
        var element = IMGS[index];
        if( index % 2  != 1 ) {
          doc.addImage(element.base,  countX,  countY , 8, 6);
          countX = countX + 8.2;
        }else{
          doc.addImage( element.base ,  'PNG',  countX,  countY , 8, 6, 'ultrasonido');
          countY = countY + 6.1
          countX = 2
        }
      }
    }
    
    this.spinner.hide()
    window.open(doc.output('bloburl', '_blank'));
    // doc.save('pdf_USG.pdf')
  }

}
