import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServiciosService } from 'src/app/services/serviciosInt/servicios.service';

@Component({
  selector: 'app-bitacora-patologias',
  templateUrl: './bitacora-patologias.component.html',
  styleUrls: ['./bitacora-patologias.component.css']
})
export class BitacoraPatologiasComponent implements OnInit {
  public consultas:any=[]
  public totalpaciente:string;
  public pagina = 0;
  public sede = '';

  constructor(private spinner : NgxSpinnerService,
              private _servicios: ServiciosService) { }

  ngOnInit(): void {
    this.sede = localStorage.getItem('cede')
    this.spinner.show()
    this.obtenerCosnultaUltra()
  }

  obtenerCosnultaUltra(){
    this._servicios.getBitacoraLabs('patologia',this.sede)
    .subscribe((data:any) => {
      if( data.ok ) {
        this.setDataUltras( data.data )
        this.spinner.hide();
      }
    })
  }

  setDataUltras(data:any) {
    this.consultas = data.reverse();
  }
}
