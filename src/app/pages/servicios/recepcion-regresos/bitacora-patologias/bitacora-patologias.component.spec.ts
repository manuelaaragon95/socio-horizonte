import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraPatologiasComponent } from './bitacora-patologias.component';

describe('BitacoraPatologiasComponent', () => {
  let component: BitacoraPatologiasComponent;
  let fixture: ComponentFixture<BitacoraPatologiasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraPatologiasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraPatologiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
