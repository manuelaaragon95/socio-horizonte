import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraEndoscopiaComponent } from './bitacora-endoscopia.component';

describe('BitacoraEndoscopiaComponent', () => {
  let component: BitacoraEndoscopiaComponent;
  let fixture: ComponentFixture<BitacoraEndoscopiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraEndoscopiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraEndoscopiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
