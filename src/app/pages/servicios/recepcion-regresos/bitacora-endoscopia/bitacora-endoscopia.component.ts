import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServiciosService } from 'src/app/services/serviciosInt/servicios.service';

@Component({
  selector: 'app-bitacora-endoscopia',
  templateUrl: './bitacora-endoscopia.component.html',
  styleUrls: ['./bitacora-endoscopia.component.css']
})
export class BitacoraEndoscopiaComponent implements OnInit {
  public consultas:any=[]
  public totalpaciente:string;
  public pagina = 0;
  public pedidosEndos= [];
  public sede = '';

  constructor(private spinner : NgxSpinnerService,
              private _servicios: ServiciosService) { }

  ngOnInit(): void {
    this.sede = localStorage.getItem('cede')
    this.spinner.show()
    this.obtenerCosnultaEndos()
  }

  obtenerCosnultaEndos(){
    this._servicios.getBitacoraLabs('endoscopia',this.sede)
    .subscribe((data:any) => {
      if( data.ok ) {
        this.setDataUltras( data.data )
        this.spinner.hide();
      }
    })
  }

  setDataUltras(data:any) {
    console.log(data);
    
    this.consultas = data.reverse();
  }
}
