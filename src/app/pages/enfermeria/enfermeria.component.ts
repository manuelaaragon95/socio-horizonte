import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { WsLoginService } from 'src/app/services/sockets/ws-login.service';

import jsPDF from 'jspdf';
import 'jspdf-autotable';

@Component({
  selector: 'app-enfermeria',
  templateUrl: './enfermeria.component.html',
  styleUrls: ['./enfermeria.component.css']
})
export class EnfermeriaComponent implements OnInit {

  public listaEspera = [];
  public getPacienteSotageX = this.listaEspera;
  public fechatl;
  public totalpaciente:string;
  public pagina = 0;
  public noSe = [];
  public imprimir = {
    indice: 0,
    nombre: '',
    curp:  '',
    edad: '',
    servMed: '',
    medico: '',
    horaCita: '',
  }
  public sede = ''

  constructor(
    public _consultasService: ConsultaService,
    private _router: Router,
    public _WsloginService: WsLoginService
  ) { }

  ngOnInit(): void {
    this.sede = localStorage.getItem('cede')
    this.obtenerConsultas();
    this._WsloginService.escucharConsulta().subscribe(arg => {
      if(arg != ""){
        this.obtenerConsultas();
      }
    });
  }

  cambioDeEstadoConsulta(  id: string ){
    let estado = {
      status : 'Enfermeria'
    }
    this._consultasService.cambiarEstadoConsulta( id, estado  )
    .subscribe( (data) => data);
  }


  obtenerConsultas(){
    this._consultasService.verConsultasPendientesEnfermeria(this.sede)
    .subscribe( (data) => {
      let pacientes = [];
      let sede = localStorage.getItem('cede')
      data['data'].forEach(element => {
        if(element.sede == sede){
          pacientes.push(element)
        }
      });
      this.listaEspera =  pacientes.reverse();
      this.totalpaciente = data['data'].results;
      this.setPaciente( pacientes);
    });
  }

  setPaciente(data) {
    let i= 0;
    for (let element of data) {    
      this.imprimir.indice= i + 1;
      this.imprimir.nombre = element.paciente.nombrePaciente;
      this.imprimir.curp = element.paciente.curp;
      this.imprimir.edad = element.paciente.edad;
      this.imprimir.servMed = element.tipoDeServicio;
      this.imprimir.medico = element.doctorAPasar;
      this.imprimir.horaCita = element.horaIngreso;
      this.noSe.push(this.imprimir)
      this.imprimir = {
        indice: 0 ,
        nombre: '',
        curp:  '',
        edad: '',
        servMed: '',
        medico: '',
        horaCita: '',
      }
      i++
    }
  }

  imprimirBitacora(){
    let values: any;
    values = this.noSe.map((elemento) => Object.values(elemento));
    const doc:any = new jsPDF();
    doc.autoTable({
      head: [['#', 'Nombre', 'Curp', 'Edad', 'Servicio Médico', 'Médico', 'Hora de Cita' ]],
      body: values
    })
    doc.save('Bitácora_Hoja_Diaria_Enf_Gral_'+this.fechatl+'_.pdf')
  }

}
