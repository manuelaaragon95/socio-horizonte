import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { PacientesService } from '../../../services/pacientes/pacientes.service';
import {   IdentificacionService } from 'src/app/services/consultas/consulta/identificacion.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { WsLoginService } from 'src/app/services/sockets/ws-login.service';
import {  ConsultaService } from 'src/app/services/consultas/consulta.service';
import { NgxSpinnerService } from "ngx-spinner";
import { SedesService } from 'src/app/services/sedes/sedes.service';
import { alerta2 } from 'src/app/clases/helpers/alerta';

moment.locale('es');

@Component({
  selector: 'app-consuta',
  templateUrl: './consultas.component.html',
  styleUrls: ['./consultas.component.css']
})
export class ConsultasComponent implements OnInit {

  public nombrePaciente ="";
  public pacientes:[]=[];
  public idPaciente="";
  public fechaIngreso = moment().format('l');
  public horaIngreso = moment().format('LTS');
  public servicio = "";
  public consultorio = "";
  public AllPacientes = [];
  public consultasCount = {
    consultas: 0
  }
  public PACIENTES_SERVICES:[]=[];
  public paciente = {

    // los datos del paciente son solo representacionales
    nombrePaciente:"",
    apellidoPaterno:"",
    apellidoMaterno:"",
    genero:"",
    curp: "",
    id:"",
    fechaIngreso:this.fechaIngreso,
    horaIngreso: this.horaIngreso,
    enfermeraAtendio:  "" ,
    diagnosticoActual: "",
    notaDeLaEnfermera: "",
    consultorio:"",
    doctorAPasar:'',
    consultas: 0,
    servicio:'',
    motivoIngreso:'',
    notaRecepcion:'',
    turno:'Matutino',
    sede:''
  }
  public listaEspera = [];
  public nombreSocio = {};
  public pacienteSeleccionado = {}
  public sede = '';
  public doctoresCuatla:any[]=[];
  @ViewChild('nombre') nombreInput: any;

  constructor(public _sede: SedesService,
              public _pacienteService: PacientesService,
              public _route: ActivatedRoute,
              public _wsLoginService: WsLoginService,
              public _consultaService: IdentificacionService,
              public _consultasService: ConsultaService,
              public _router:Router,
              private spinner: NgxSpinnerService){}

  ngOnInit(): void {
    this.nombreSocio = JSON.parse(localStorage.getItem('usuario')).nombre
    this.idPaciente =  this._route.snapshot.paramMap.get('id');
    this.sede = localStorage.getItem('cede')
    if (this.sede == 'CTLA01') {
      this._sede.getMedicos(this.sede).subscribe((resp:any)=>{
        if (resp.ok) {
          this.doctoresCuatla = resp['data']
        }
      })
    }
    this.pacienteSeleccionado = JSON.parse(localStorage.getItem('paciente'))
    if (this.pacienteSeleccionado != undefined) {
      this.seleccionarPaciente(this.pacienteSeleccionado)
    }
    this.obtenerConsultas();
  }

  obtenerPacientes() {
    this._pacienteService.getPacientesAll()
    .subscribe( (data:any) => {
      if(data.ok) {
        this.PACIENTES_SERVICES = data['data'];
      }
    });
  }

  obtenerPacientePorId(){
    this._pacienteService.getPacienteBtID(  this.idPaciente )
    .subscribe((data:any) => {
      this.paciente.nombrePaciente = data['paciente']['nombrePaciente'];
      this.paciente.apellidoPaterno = data['paciente']['apellidoPaterno'];
      this.paciente.apellidoMaterno = data['paciente']['apellidoMaterno'];
      this.paciente.curp = data['paciente']['curp'];
      this.paciente.genero = data['paciente']['genero']
      this.paciente.id = data['paciente']['_id'];
    });
  }

  seleccionarConsultprop( consultorio  ) {
    let selectConsultorio = document.getElementById('consultorioSelect');
    if( consultorio['value']   == "seleccionarConsultorio" ){
      alerta2('warning', 'Selecciona un consultorio');
      selectConsultorio.classList.add('is-invalid')
      return;
    }
    this.consultorio = consultorio;
  }


  seleccionarPaciente( item ) {
    this.paciente.nombrePaciente = item['nombrePaciente'];
    this.paciente.apellidoPaterno = item['apellidoPaterno'];
    this.paciente.apellidoMaterno = item['apellidoMaterno'];
    this.paciente.curp = item['curp'];
    this.paciente.genero = item['genero'];
    this.paciente.consultas = item['consultas'];
    this.paciente.id = item['_id'];
    this.PACIENTES_SERVICES = [];
    this.AllPacientes = [];
    this.nombreInput.nativeElement.value = '';
  }

  buscarPaciente(  nombre: string ){
    if(nombre.length >= 3){
      this._pacienteService.buscarPacienteSede(this.sede, {nombre: nombre}).subscribe( (data: any) =>{
        this.AllPacientes = data.data
      });
    }else{
      this.AllPacientes = [];
    }
  }

  actualizarConteoConsultas(){
    this.consultasCount.consultas = this.paciente.consultas = this.paciente.consultas + 1;
    this._consultasService.actualizarConteoConsultas(  this.paciente.id, this.consultasCount )
    .subscribe( data => console.log(data));
  }

  agregarSede(){
    if(localStorage.getItem('cede') != undefined){
      this.paciente.sede = localStorage.getItem('cede');
    }
  }

  enviar(){
    this.spinner.show();
    this.agregarSede();
    this._consultaService.agregarConsulta(  this.paciente )
    .subscribe((data) => {
      if( data['ok'] ){
        this._wsLoginService.enviarConsultas(  data['data']['_id'] );
        alerta2('success', 'El paciente pasó a enfermería');
        this.actualizarConteoConsultas();
        this.PACIENTES_SERVICES = [];
        this.spinner.hide();
        this._router.navigateByUrl('/');
      }
    }, error =>{
      console.log(error);
      this.spinner.hide();
      alerta2('error', 'No se pudo generar la consulta');
    });
  }
  obtenerConsultas(){
    this._consultasService.verConsultasPendientesEnfermeria(this.sede)
    .subscribe( (data) => {
      this.listaEspera = data['data']
    });
  }
}
