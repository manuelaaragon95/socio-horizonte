import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { HistoriaClinicaService } from 'src/app/services/historiaClinica/historia-clinica.service';
import { WsLoginService } from 'src/app/services/sockets/ws-login.service';


@Component({
  selector: 'app-ficha-enfermeria',
  templateUrl: './ficha-enfermeria.component.html',
  styleUrls: ['./ficha-enfermeria.component.css']
})
export class FichaEnfermeriaComponent implements OnInit {

  public id: string;
  public fecha: string;
  public paciente  = {
    nombrePaciente:"",
    apellidoPaterno:"",
    apellidoMaterno: "",
    fechaNacimientoPaciente:"",
    curp:"",
    telefono:0,
    consultas: 0,
    _id:"",
    fechaRegistro:Date,
    genero:"",
    estadoPaciente:"",
    callePaciente:"",
    paisPaciente:"",
    cpPaciente:"",
    contactoEmergencia1Nombre:"",
    contactoEmergencia1Edad:"",
    contactoEmergencia1Telefono:"",
    correo: "",
    edad:"",
    paquetes : [],
    membresiaActiva:false,
    numeroExpediente:'',
    religion:'',
    tipoDeSangre:''
  };
  public mostrarHistoria = false;

  constructor(private _HistoriaClinicaService: HistoriaClinicaService,
              private _route: ActivatedRoute,
              public _consultaService: ConsultaService,
              public _WsLoginService: WsLoginService) {}

  ngOnInit(): void {
    this.id = this._route.snapshot.paramMap.get('id');
    this.fecha = moment().format('l');
    this.obtenerConsultaPorId();
  }

  activarHistoriaClinica(event:any){
    if (event.target.value == 'SI') {
      this.mostrarHistoria = true;
    } else if (event.target.value == 'NO'){
      this.mostrarHistoria = false;
    }
  }

  obtenerConsultaPorId(){
    this._HistoriaClinicaService.obtenerConsultaPorElId( this.id )
    .subscribe((resp:any) => { 
      this.setPaciente(resp['data'].paciente)
    });
  }

  setPaciente(data){
    for (const key in this.paciente) {
      if (data[key] == undefined) {
        this.paciente[key] = ''
      }else{
        this.paciente[key] = data[key]
      }
    }
  } 
}
