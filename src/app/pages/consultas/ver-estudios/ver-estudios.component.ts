
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { RecetasService } from 'src/app/services/recetas/recetas.service';
import { getCarritoStorage } from '../../../functions/storage/pacienteIntegrados';
import { gaurdarCotizacion } from '../../../functions/storage/storage.functions';
import CarritoMembresia from 'src/app/clases/carritoMembresía/carritoMembresia.class';
/* import PacienteStorage from 'src/app/classes/pacientes/pacientesStorage.class'; */
import Carrito from 'src/app/clases/carrito/carrito.class';
import CarritoSocios from '../../../clases/carrito-socios/socios-carrito';
import jsPDF from 'jspdf';
import { ServiciosService } from '../../../services/serviciosInt/servicios.service';
import { filterServicios } from './../../../clases/buscarServicos/buscarServicos';

@Component({
  selector: 'app-ver-estudios',
  templateUrl: './ver-estudios.component.html',
  styleUrls: ['./ver-estudios.component.css']
})
export class VerEstudiosComponent implements OnInit {

  public id: string;
  public carrito = {
    totalSin: 0,
    totalCon: 0,
    items: []
  };
  public paciente = {
    nombrePaciente:"",
    apellidoPaterno:"",
    apellidoMaterno: "",
    curp:"",
    telefono:0,
    consultas: 0,
    _id:"",
    fechaRegistro:Date,
    genero:"",
      estadoPaciente:"",
      callePaciente:"",
      paisPaciente:"",
      cpPaciente:"",
    contactoEmergencia1Nombre:"",
    contactoEmergencia1Edad:"",
    correo: "",
    cp:"",
    edad:"",
    paquetes : [],
    membresiaActiva:false
  }

  public recetMedica = {
    folio:'',
    medicamentos: [],
    estudios: [],
    otrasIndicaciones: "",
    idPaciente: "",
    idConsulta: "",
    fechaReceta: '',
    horareceta: '',
    medicoQueAtendio: '',
    idMedico: '',
    cedula: '',
    especialidad:'',
    cedulaEsp:'',
    firma: '',
    origen: '',
    horaEgreso: '',
    universidad:'',
    prioridad: 'Programado'
  }

  public recetaPDF= {
    height:3.7,
    width:1.5,
    whithEspacio: 3.4
  }

  public talla: any
  public peso:any
  public temp:any
  public diagnostico = []

  public estudios:any[] =[];
  
  public totalSinMembresia = 0;
  public totalConMembresia = 0;
  public totalCarritoMembresia: Number;
  public idSede = ''
  public allServicios:any [] = [];

  constructor(private _recetaService: RecetasService,
              private _route: ActivatedRoute,
              private _service: ServiciosService) {  }
    
  ngOnInit(): void {
    this.idSede = localStorage.getItem('IdSede')
    this.id = this._route.snapshot.paramMap.get('id');
    this.obtenerReceta();
    this.obtenerAllServicios();
  }

  obtenerAllServicios(){
    let val = {
      idSede : this.idSede
    }
    this._service.obtenerServiciosSede(val).subscribe((resp:any) =>{
      if(resp.ok){
        this.allServicios = resp['data']
      }
    })
  }

  busquedaGeneral(event:any){
    if (event.target.value == '') {
      this.estudios = []
      this.obtenerReceta();
    } else if (event.target.value.length >= 3){
      this.estudios = []
      this.estudios= filterServicios(this.allServicios, event.target.value)
    }
  }

  obtenerCarrito(){
    let carritoSinMembresia = new CarritoSocios();
    this.carrito = carritoSinMembresia.obtenerSotorageCarrito();
      // console.log( this.carrito );
    this.totalSinMembresia = this.carrito.totalSin;
    this.totalCarritoMembresia= this.carrito.totalCon;
  }

  obtenerReceta(){
    this._recetaService.verRecetaConConsulta(  this.id )
    .subscribe(  data => {
      console.log(data);
      
      this.setDataReceta(data['data'])
      this.paciente.nombrePaciente = data['data']['idPaciente'].nombrePaciente;
      this.paciente.apellidoPaterno = data['data']['idPaciente'].apellidoPaterno;
      this.paciente.apellidoMaterno = data['data']['idPaciente'].apellidoMaterno;
      this.paciente.cp = data['data']['idPaciente'].cpPaciente;
      this.paciente.estadoPaciente = data['data']['idPaciente'].estadoPaciente;
      this.paciente.paisPaciente = data['data']['idPaciente'].paisPaciente;
      this.paciente.callePaciente = data['data']['idPaciente'].callePaciente;
      this.paciente.cpPaciente = data['data']['idPaciente'].cpPaciente;
      this.paciente.edad = data['data']['idPaciente'].edad;
      this.paciente.correo = data['data']['idPaciente'].correoPaciente;
      this.paciente.curp = data['data']['idPaciente'].curp;
      this.paciente._id = data['data']['idPaciente']._id;
      this.paciente.paquetes = data['data']['idPaciente'].paquetes;
      this.paciente.contactoEmergencia1Nombre = data['data']['idPaciente'].contactoEmergencia1Nombre;
      this.paciente.contactoEmergencia1Edad = data['data']['idPaciente'].contactoEmergencia1Edad;
      this.paciente.genero = data['data']['idPaciente'].genero;
      this.paciente.fechaRegistro = data['data']['idPaciente'].fechaRegistro;
      this.obtenerServiciosUti(data['data']['estudios'])
      /* this.estudios = data['data']['estudios']; */
      /* this.setMembresia( data['data']['idPaciente']['membresiaActiva'] );
      this.setLocalStoragePaciente( data['data']['idPaciente'] ); */
      this.guardarLocalStorage();
    });
  }

  setDataReceta(receta:any){
    this.recetMedica.cedula = receta.idMedico.cedulaProfesional
    this.recetMedica.cedulaEsp = receta.idMedico.cedulaEsp
    this.recetMedica.especialidad = receta.idMedico.Especialidad
    this.recetMedica.estudios = receta.estudios
    this.recetMedica.fechaReceta = receta.fechaReceta
    this.recetMedica.medicamentos = receta.medicamentos
    this.recetMedica.medicoQueAtendio = receta.idMedico.medico
    this.recetMedica.otrasIndicaciones = receta.otrasIndicaciones
    this.recetMedica.universidad = receta.idMedico.universidad
    this.recetMedica.folio = receta.folio
    if (this.recetMedica.cedula == undefined) {
      this.recetMedica.cedula = ""
    }
    if (this.recetMedica.cedulaEsp == undefined) {
      this.recetMedica.cedulaEsp = ""
    }
    if (this.recetMedica.medicoQueAtendio == undefined){
      this.recetMedica.medicoQueAtendio = receta.idMedico.nombre
    }
    if (this.recetMedica.folio == undefined) {
      this.recetMedica.folio = ""
    }
  }

  obtenerServiciosUti(datos){
    this.estudios = []
    datos.forEach(element => {
      this._recetaService.obtenerEstudioUtilidad(element._id,this.idSede).subscribe((resp:any)=>{
        if (resp['data'].length > 0) {
          this.estudios.push(resp['data'][0])
        }
      })
    });
  }

  setMembresia( status ){
    this.paciente.membresiaActiva = status;
    this.obtenerCarrito();
  }

  guardarLocalStorage(){
    if(  localStorage.getItem('paciente') ){
      localStorage.removeItem('paciente');
    }
    if(  this.paciente.paquetes.length >= 1 || this.paciente.membresiaActiva == true ){
        this.paciente.membresiaActiva = true;
    }
    localStorage.setItem('paciente', JSON.stringify(this.paciente));
    /* console.log(this.paciente); */
  }

  setLocalStoragePaciente( paciente ){
    /* this.paciente = paciente
    this.guardarLocalStorage(this.paciente); */
    /* localStorage.setItem('idPaciente', JSON.stringify(paciente)) */
    /* const pacienteStorage = new PacienteStorage();
    const result  =  pacienteStorage.setPacienteStorage( idPaciente ); */
    // if( result  ){
    //   console.log("paciente agregado");
    // }
  }

  alertcomparasion( ev, precioPublicoOtros, precioMembresia, item2:any ){
    let precioSinTrim  =  precioPublicoOtros.replace('$', '');
    let precioSinComaPublico = precioSinTrim.replace(',', '');
    let precioMemTrim  =  precioMembresia.replace('$', '');
    let precioMemComaMembresia = precioMemTrim.replace(',', '');
    Swal.fire({
      icon: 'success',
      title: 'CON MEMBRESIA THE AHORRAS',
      text: `$ ${precioSinComaPublico - precioMemComaMembresia}.00`,
    })
    this.agregarCarrito(precioPublicoOtros, precioMembresia, item2);
  }
  //carrito
  agregarCarrito( peciosSin,precioCon, item: any ) {
    let carrito = new CarritoSocios();
    this.carrito = carrito.agregarItem(  peciosSin, precioCon, item);
    this.obtenerCarrito()
  }

  eliminar( id ) {
    let carrito = new CarritoSocios();
    carrito.eliminar( id );
    this.obtenerCarrito();
    // this.obtenerCarritoStorage();
  }

  imprimirFrente() {
    const tiempoTranscurrido = Date.now();
    const hoy = new Date(tiempoTranscurrido);
    /*HOJA POR EL FRENTE*/
    let imgSocios = '../../assets/images/socio_horizonte.png';
    let linea1 = '../../assets/images/piePaginaTlaya.png';
    let linea2 = '../../assets/images/linea 2.png';
    let marca = '../../assets/images/marcaAguaSocio.png';
    let universidad = this.recetMedica.universidad
    let hora_impresion = hoy.toLocaleDateString()
    const direccion =  `${this.paciente.callePaciente ? this.paciente.callePaciente : ''} ${this.paciente.estadoPaciente ? this.paciente.estadoPaciente : ''}`;
    const edad = this.paciente.edad ? this.paciente.edad : '';
    const genero = this.paciente.genero ? this.paciente.genero : '';
    const doc:any = new jsPDF({
      unit: "cm",
      format: 'a4',
    });
    doc.addImage(imgSocios, 'PNG', 1, 0.5,6,2);
    doc.addImage(linea2, 'PNG', 10, 1.5);
    doc.addImage(marca, 'PNG', 4.5, 8, 12, 12);
    doc.setFontSize(16);
    doc.setFont('helvetica')
    doc.text('RECETA MÉDICA', 8, 4)
    doc.text('FOLIO: ' + this.recetMedica.folio, 16,4)
    doc.setFontSize(12);
    doc.text('FECHA DE CONSULTA:', 12.5, 5)
    doc.text(`${this.recetMedica.fechaReceta}`,17.5,5)
    doc.text('NOMBRE:', 1, 6)
    doc.text(`${this.paciente.nombrePaciente} ${this.paciente.apellidoPaterno} ${this.paciente.apellidoMaterno}`, 3.2, 6)
    doc.text('GÉNERO: ', 1, 6.7);
    doc.text(`${genero}`, 3.2, 6.7);
    doc.text('EDAD: ', 7, 6.7);
    doc.text(`${edad}` + ' AÑOS', 8.5, 6.7);
    doc.text('DIRECCIÓN:', 1, 7.5)
    doc.text(`${direccion}`,3.7, 7.5)
    doc.text('MEDICAMENTOS:', 1, 9)
    let a = 9.8;
    let b = 10.7;
    this.recetMedica.medicamentos.forEach((medicamento: any, index) => {
      this.recetaPDF.height += 1;
      doc.text(1, a, (this.recetaPDF.width, this.recetaPDF.height, (index + 1) + '.-' + `${medicamento.medicamento}`),{ maxWidth: 18 });
      a = a + 1.8;
      this.recetaPDF.height += 1;
      doc.text(2, b,'-'+ (this.recetaPDF.width, this.recetaPDF.height, `${medicamento.indicaciones.toUpperCase()}`),{ maxWidth: 18 });
      b = b + 1.8;
    });

    doc.text('OTRAS INDICACIONES:', 1, b)
    b = b + .5

    // doc.text(`${this.recetMedica.otrasIndicaciones.toLocaleUpperCase()}`, 1, b, { maxWidth: 19, align: 'justify' })
    let lines = doc.setFont('helvetica')
    .setFontSize(12)
    .splitTextToSize(this.recetMedica.otrasIndicaciones.toLocaleUpperCase(), 19)
    
    lines.forEach(element => {
      doc.text(element, 1, b,{ align: 'justify' })
      b = b +.5
    });
    
    doc.text("MEDICO: " + this.recetMedica.medicoQueAtendio.toUpperCase(), 7, 24.5);

    doc.text("ESPECIALIDAD: " + (this.recetMedica.especialidad ? this.recetMedica.especialidad.toLocaleUpperCase() : ''), 7, 25);

    doc.text("CÉDULA: " + this.recetMedica.cedula + "/" + this.recetMedica.cedulaEsp, 7, 25.5);
    /* doc.text("UNIVERSIDAD: ", 8,26); */
    console.log(universidad);
    
    if(universidad != undefined && universidad != null && universidad != ''){
      doc.addImage(universidad, 'PNG', 16.5, 23, 3, 3);
    }
    
    if (b > 21) {
      b = 8
      doc.addImage(linea1, 'PNG', 1, 26.5, 19, 1.4);
      doc.setFontSize(8);
      doc.setTextColor(0, 0, 0);
      doc.text("Horario: Lunes a Domingo 24 horas", 1, 28.1);
      doc.text("Fancisco I. Madero #27 Barrio del Rosario Tlayacapan, Mor.", 1, 28.4);
      doc.text("Tel. (735) 357 7564", 1, 28.7);
      doc.text("atencion.horizonte@gmail.com", 1, 29);
      doc.setFontSize(12);
      doc.addPage();
      doc.addImage(imgSocios, 'PNG', 1, 0.5, 6, 2);
      doc.addImage(linea2, 'PNG', 17, 0.5, 2, 2);
      doc.addImage(marca, 'PNG', 1, 6, 19, 25);
      doc.setFontSize(16);
      doc.setFont('helvetica')
      doc.text('RECETA MÉDICA', 8, 4)
      doc.text('FOLIO: ' + this.recetMedica.folio, 16, 4)
      doc.setFontSize(12);
      doc.text('FECHA DE CONSULTA:', 12.5, 5)
      doc.text(`${this.recetMedica.fechaReceta}`, 17.5, 5)
      doc.text('NOMBRE:', 1, 6)
      doc.text(`${this.paciente.nombrePaciente} ${this.paciente.apellidoPaterno} ${this.paciente.apellidoMaterno}`, 3.2, 6)
      doc.text('GÉNERO: ', 1, 6.7);
      doc.text(`${this.paciente.genero}`, 3.2, 6.7);
      doc.text('EDAD: ', 7, 6.7);
      doc.text(`${this.paciente.edad}` + ' AÑOS', 8.5, 6.7);
      doc.text('DIRECCIÓN:', 1, 7.5)
      doc.text(`${this.paciente.callePaciente} ${this.paciente.estadoPaciente}`, 3.7, 7.5)
    }
    b = b + 1
    if (this.recetMedica.estudios.length != 0) {
      doc.text('ESTUDIOS:', 1, b)
    }
    // let c = 20.5
    this.recetMedica.estudios.forEach((estudio, i) => {
      doc.text(`${estudio.ESTUDIO}`, 1, (b + 0.5));
      b = b + .5;
    })
    doc.addImage(linea1, 'PNG', 1, 26.5,19,1.4);
    doc.setFontSize(8);
    doc.setTextColor(0,0,0);
    doc.text("Horario: Lunes a Sábado de 8:00 a.m. a 8:00 p.m", 1,28.1);
    doc.text("Ignacio Maya 252-258, Emiliano Zapata, 62744 Cuautla, Mor.", 1,28.4);
    doc.text("Tel. (735) 357 7564 / 357 67 85 ext. 101", 1,28.7);
    doc.text("atencion.horizonte@gmail.com", 1,29);
    //doc.output('dataurlnewwindow');
    /* doc.save(this.paciente.nombrePaciente + " " + this.paciente.apellidoPaterno); */
    window.open(doc.output('bloburl', '_blank'));
  }

}
