import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { getDataStorage } from 'src/app/functions/storage/storage.functions';
import { HistoriaClinicaService } from 'src/app/services/historiaClinica/historia-clinica.service';
import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { Cie10Service } from 'src/app/services/consultas/consulta/cie10.service';
import { ServiciosService } from "src/app/services/admin/servicios.service";
import * as moment from 'moment'
import { NgxSpinnerService } from "ngx-spinner";
import { alerta, alerta2 } from 'src/app/clases/helpers/alerta';

@Component({
  selector: 'app-hevolucion-cg',
  templateUrl: './hevolucion-cg.component.html',
  styleUrls: ['./hevolucion-cg.component.css']
})
export class HevolucionCgComponent implements OnInit {
  public fecha: string;
  public hora: string;
  public id: string;
  public buscarDiagnostico: string;
  public diagnosticos: [] = [];

  // se le puso hoja de evilucion al objeto pero se llama historia clinca por aparatos y sistemas
  public antecedentesPersonalesNoPatologicos = {
    tabaquismoPorDia: "",
    aniosConsumo: "",
    exFumadorPasivo: "",
    alcoholismoPorDia: "",
    aniosDeconsumoAlcohol: "",
    exAlcoholicoUOcasional: "",
    alergias: "",
    tipoAlergias: "",
    tipoSanguineo: "",
    desconoceTipoSanguineo: "",
    drogadiccionTipo: "",
    aniosConsumoDrogas: "",
    exDrogadicto: "",
    alimentacionAdecuada: "",
    viviendaConServiciosBasicos: "",
    otrosAntecedentesNoPatologicos: "",
    idPaciente: ""
    }
    
  public pageTitle: 'ddd';

  public hojaEvolucion = {
    motivoDeConsulta:'',
    evolucionDelPadecimiento:'',
    medicoTrante:'',
    diagnosticos:[],
    plan:'',
    respiratorioCardiovascular:"",
    digestivo:"",
    endocrino:"",
    musculoEsqueletico:"",
    genitourinario:"",
    hematopoyeticoLinfatico:"",
    pielAnexos:"",
    neurologicoPsiquiatricos:"",
    piel:'',
    cabezaCuello:"",
    torax:"",
    abdomen:"",
    genitales:"",
    extremidades:"",
    sistemaNervioso:"",
    status:""
  }
  public edad:number=0;

  public necistaReceta = "";

  // este json nos ayuda en la representacion de los datos
  public paciente = {
    nombre: '',
    apellidoPaterno: '',
    apellidoMaterno: '',
    edad: 0,
    registro: '',
    genero: '',
    consultas:0,
    medico: '',
    calle:"",
    curp:"",
    idPaciente:"",
    municipio:"",
    talla: '',
    peso: '',
    imc: '',
    fc: '',
    ta:"",
    fr: '',
    lpm:'',
    sistolica: '',
    diastolica: '',
    temp: '',
    glucosa: '',
    pc: '',
    pa: '',
    pt: '',
    apgar: 0  ,
    sao: '',
    rpm:'',
    alergias: '',
    pao: '',

  }

  public paciente2  = {
    nombrePaciente:"",
    apellidoPaterno:"",
    apellidoMaterno: "",
    fechaNacimientoPaciente:"",
    curp:"",
    telefono:0,
    consultas: 0,
    _id:"",
    fechaRegistro:Date,
    genero:"",
    estadoPaciente:"",
    callePaciente:"",
    paisPaciente:"",
    cpPaciente:"",
    contactoEmergencia1Nombre:"",
    contactoEmergencia1Edad:"",
    contactoEmergencia1Telefono:"",
    correo: "",
    edad:"",
    paquetes : [],
    membresiaActiva:false,
    numeroExpediente:'',
    religion:'',
    tipoDeSangre:''
  };





  public tallatl;
  public tallaPrueba = [];


    // Grafica Mamalona de peso
    // public lineChartData: ChartDataSets[] = [
    //   { data: [1.2, 1.3, 1.32, 1.32, 1.34, 1.34, 1.35, 1.40, 1.45, 1.50, 1.50, 1.50, 1.50, 1.52], label: 'Peso Mínimo' }

    // ];

    // public lineChartLabels: Label[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14'];

    // public lineChartOptions: (ChartOptions & { annotation: any }) = {
    //   responsive: true,
    //   scales: {
    //     // We use this empty structure as a placeholder for dynamic theming.
    //     xAxes: [{}],
    //     yAxes: [
    //       {
    //         id: 'y-axis-0',
    //         position: 'left',
    //       },
    //       {
    //         id: 'y-axis-1',
    //         position: 'right',
    //         gridLines: {
    //           color: 'rgba(0,0,2555,0.2)',
    //         },
    //         ticks: {
    //           fontColor: 'black',
    //         }
    //       }
    //     ]
    //   },
    //   annotation: {
    //     annotations: [
    //       {
    //         type: 'line',
    //         mode: 'vertical',
    //         scaleID: 'x-axis-0',
    //         value: 'March',
    //         borderColor: 'orange',
    //         borderWidth: 2,
    //         label: {
    //           enabled: true,
    //           fontColor: 'orange',
    //           content: 'LineAnno'
    //         }
    //       },
    //     ],
    //   },
    // };
    // public lineChartColors: Color[] = [
    //   { // grey
    //     // backgroundColor: 'rgba(148,159,177,0.2)',
    //     borderColor: 'rgba(255,0,0,0.6)',
    //     pointBackgroundColor: 'rgba(255,0,0,0.6)',
    //     pointBorderColor: 'rgba(255,0,0,0.6)',
    //     // pointHoverBackgroundColor: '#fff',
    //     // pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    //   },
    //   { // dark grey
    //     // backgroundColor: 'rgba(0,0,255,1)',
    //     borderColor: 'rgba(0,0,255,1)',
    //     pointBackgroundColor: 'rgba(0,0,255,1)',
    //     pointBorderColor: 'rgba(0,0,255,1)',
    //     // pointHoverBackgroundColor: '#fff',
    //     // pointHoverBorderColor: 'rgba(77,83,96,1)'
    //   },
    //   { // red
    //     // backgroundColor: 'rgba(0,255,0,0.6)',
    //     borderColor: 'green',
    //     pointBackgroundColor: 'rgba(0,255,0,0.6)',
    //     pointBorderColor: 'green',
    //     // pointHoverBackgroundColor: 'green',
    //     // pointHoverBorderColor: 'rgba(0,255,0,0.6)'
    //   }
    // ];
    // public lineChartLegend = true;
    // public lineChartType = 'line';
    // public lineChartPlugins = [];

    // @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
  // FIN GRAFICA Mamalona

  public otrosEstudios:any[] = [];
  public endos:any[] = [];
  public obtenidosLab= [];
  public resultado:any[]=[];
  public listaOtrosServ:any[]=[
    {
      nombre:'LABORATORIO'
    },{
      nombre:'ULTRASONIDO'
    },{
      nombre:'RAYOSX'
    },{
      nombre:'ENDOSCOPIA'
    },{
      nombre:'TOMOGRAFIA'
    },{
      nombre:'OTROS ESTUDIOS'
    },{
      nombre:'NOTAS Y RESUMENES'
    },{
      nombre:'RECETAS'
    }
  ];

  constructor(
    private _route: ActivatedRoute,
    private _HistoriaClinicaService: HistoriaClinicaService,
    public _consultaService: ConsultaService,
    private router: Router,
    public _Cie10Service: Cie10Service,
    private spinner: NgxSpinnerService,
    private _servicios: ServiciosService 
  ) { }

  ngOnInit(): void {
    this.fecha = moment().format('L');
    this.necistaReceta = 'si'
    // OBTENEMOS LA INFORMCION DEL ROL
      this.hojaEvolucion.medicoTrante = getDataStorage().nombre;
    // Obtener Id del Paciente
    this.id = this._route.snapshot.paramMap.get('id');
    this.obtenerConsulta();
    // this.setEdad();
  }

  alertaGraficas(){
    const texto = 'Al parecer es la primer visita del paciente, no hemos encontrado Historico de Signos Vitales'
    alerta2('warning', texto)
  }

  // setEdad(){
  //   this.edad = parseFloat(this.paciente.edad);
  // }

  //delete de los diagnosticos de la receta
  deleteItem(i:number){
    // console.log(i);
    this.hojaEvolucion.diagnosticos.splice( i, 1 );
  } 

  obtenerConsulta(){
    this._HistoriaClinicaService.obtenerConsultaPorElId( this.id  )
    .subscribe(
      (data:any) => {
        this.paciente.consultas = data['data']['paciente']['consultas'];
        this.hora = data['data'].horaIngreso;
        this.paciente.nombre = data['data']['paciente']['nombrePaciente'];
        this.paciente.apellidoPaterno = data['data']['paciente'].apellidoPaterno;
        this.paciente.apellidoMaterno = data['data']['paciente'].apellidoMaterno;
        this.paciente.edad = data['data']['paciente'].edad;
        this.paciente.curp = data['data']['paciente'].curp;
        this.paciente.idPaciente = data['data'].paciente._id;
        this.paciente.diastolica = data['data'].diastolica;
        this.paciente.sistolica = data['data'].sistolica;
        this.paciente.genero = data['data'].paciente.genero;
        this.paciente.registro = data['data']['paciente']['fechaRegistro'];
        this.paciente.calle = data['data']['paciente']['callePaciente'];
        this.paciente.talla = data['data']['talla'];
        this.paciente.temp = data['data']['temp'];
        this.paciente.peso = data['data']['peso'];
        this.paciente.pt = data['data']['pt'];
        this.paciente.pc = data['data']['pc'];
        this.paciente.imc = data['data']['imc'];
        this.paciente.lpm = data['data']['lpm'];
        this.paciente.pa = data['data']['pa'];
        this.paciente.apgar = data['data']['apgar'];
        this.paciente.sao = data['data']['SaO'];
        this.paciente.pao = data['data']['pao'];
        this.paciente.rpm =data['data']['rpm'];
        this.paciente.glucosa =data['data']['glucosa'];
        this.paciente.fc = data['data'].fc
        this.paciente.fr = data['data'].fr
        this.setPaciente(data['data']['paciente'])
      })
  }

  setPaciente(data){
    for (const key in this.paciente2) {
      if (data[key] == undefined) {
        this.paciente2[key] = ''
      }else{
        this.paciente2[key] = data[key]
      }
    }
  } 

  descargarEndos(base:any,nombre:any){
    const linkSource = base;
    const downloadLink = document.createElement("a");
    const fileName = nombre;
    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }

   //Inicio Funciones Grafica
  //  public randomize(): void {
  //   for (let i = 0; i < this.lineChartData.length; i++) {
  //     for (let j = 0; j < this.lineChartData[i].data.length; j++) {
  //       this.lineChartData[i].data[j] = this.generateNumber(i);
  //     }
  //   }
  //   this.chart.update();
  // }

  // private generateNumber(i: number) {
  //   return Math.floor((Math.random() * (i < 2 ? 100 : 1000)) + 1);
  // }

  // // events
  // public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  //   // console.log(event, active);
  // }

  // public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  //   // console.log(event, active);
  // }

  // public hideOne() {
  //   const isHidden = this.chart.isDatasetHidden(1);
  //   this.chart.hideDataset(1, !isHidden);
  // }

  // public pushOne() {
  //   this.lineChartData.forEach((x, i) => {
  //     const num = this.generateNumber(i);
  //     const data: number[] = x.data as number[];
  //     data.push(num);
  //   });
  //   this.lineChartLabels.push(`Label ${this.lineChartLabels.length}`);
  // }

  // public changeColor() {
  //   this.lineChartColors[2].borderColor = 'green';
  //   this.lineChartColors[2].backgroundColor = `rgba(0, 255, 0, 0.3)`;
  // }

  // public changeLabel() {
  //   this.lineChartLabels[2] = ['1st Line', '2nd Line'];
  //   // this.chart.update();
  // }

  // funciones de validaciones

  validarMotivoConsulta(){
    if(this.hojaEvolucion.motivoDeConsulta.length < 6 || this.hojaEvolucion.motivoDeConsulta == ''){
      return  false;
    }else {
      return true;
    }
  }

  validarEvolucionDelPaciente(){
    if(  this.hojaEvolucion.evolucionDelPadecimiento == ""  || this.hojaEvolucion.evolucionDelPadecimiento.length < 6 ){
      return false;
    }else {
      return true;
    }
  }


  validarPlan(){
    if(   this.hojaEvolucion.plan == "" || this.hojaEvolucion.plan.length < 5 ){
      return false;    
    }else {
      return true;
    }
  }

  agregarDiagnosticoUno( item  ){
    let diagnostico  = { diagnostico: item}
    this.hojaEvolucion.diagnosticos.push( diagnostico );
    this.diagnosticos = [];
    this.buscarDiagnostico = ''
  }

  obtenerDiagnosticoUno(){

    if(this.buscarDiagnostico.length > 3) {
      this._Cie10Service.getCiePorNombre(this.buscarDiagnostico).subscribe(
        (data:any) => {
            this.diagnosticos = data.data;
        });
    }

  }

// validaciones
  validacion(texto: string){
    if( texto.length == 0  ){
      return true;
    }else {
      return false;
    }

  }

  obtenerResultados(estudio:any) {
    this.spinner.show();
      this._servicios.obtenerRegresosHistoriaClinica( this.paciente.idPaciente, estudio)
      .subscribe((data:any) => {
        if(data.ok)this.setEstudio(data['data'],estudio);
      })
  }

  obtenerOtrosEstudios(servicio:any){
    this.spinner.show();
    this.otrosEstudios=[];
    this._servicios.obtenerOtrosEstudiosPaciente(this.paciente.idPaciente, servicio).subscribe((resp:any)=>{
      if(resp.ok){
        this.otrosEstudios = resp['data']
        this.spinner.hide()
      }
    })
  }

  setEstudio(estudio = [],estudioPedido) {
    // console.log(estudio);
    if(estudioPedido == 'endoscopia'){
      this.endos = estudio
      this.spinner.hide();
    }else{
      this.obtenidosLab = estudio;
      this.resultado = estudio
      // console.log(this.resultado);
      this.spinner.hide();
      if (!this.resultado[0]) {
        return;
      }
    }
  }

  alertValidacionicompleta(validacion) {
    //swal(message, { icon: "warning" });
    const { mensaje,  title} = validacion
    alerta('warning', title, mensaje);
    return false;
  }

  validarAparatosYsistemas() {
    const validaciones = [
        { campo: this.hojaEvolucion.respiratorioCardiovascular, mensaje: "Complete el campo Respiratorio/Cardiovascular", title:'Interrogación por aparatos y sistemas' },
        { campo: this.hojaEvolucion.digestivo, mensaje: "Complete el campo Digestivo", title:'Interrogación por aparatos y sistemas' },
        { campo: this.hojaEvolucion.endocrino, mensaje: "Complete el campo Endocrino", title:'Interrogación por aparatos y sistemas' },
        { campo: this.hojaEvolucion.musculoEsqueletico, mensaje: "Complete el campo Músculo-Esquelético", title:'Interrogación por aparatos y sistemas' },
        { campo: this.hojaEvolucion.genitourinario, mensaje: "Complete el campo Genitourinario", title:'Interrogación por aparatos y sistemas' },
        { campo: this.hojaEvolucion.hematopoyeticoLinfatico, mensaje: "Complete el campo Hematopoyético - Linfático", title:'Interrogación por aparatos y sistemas' },
        { campo: this.hojaEvolucion.pielAnexos, mensaje: 'Complete el campo Piel y Anexos', title:'Interrogación por aparatos y sistemas' },
        { campo: this.hojaEvolucion.neurologicoPsiquiatricos, mensaje: "Complete el campo Neurológico y Psiquiátrico", title:'Interrogación por aparatos y sistemas' },
        { campo: this.hojaEvolucion.piel, mensaje: "Complete el campo Piel y Anexos", title:'Exploración fisica' },
        { campo: this.hojaEvolucion.cabezaCuello, mensaje: 'Complete el campo Exploración Cabeza y Cuello', title:'Exploración fisica' },
        { campo: this.hojaEvolucion.torax, mensaje: 'Complete el campo Tórax', title:'Exploración fisica' },
        { campo: this.hojaEvolucion.abdomen, mensaje: 'Complete el campo Abdomen', title:'Exploración fisica' },
        { campo: this.hojaEvolucion.genitales, mensaje: 'Complete el campo Genitales', title:'Exploración fisica' },
        { campo: this.hojaEvolucion.extremidades, mensaje: 'Complete el campo Extremidades', title:'Exploración fisica' },
        { campo: this.hojaEvolucion.sistemaNervioso, mensaje: 'Complete el campo Sistema Nervioso', title:'Exploración fisica' }
    ];
    for (let validacion of validaciones) {
      if (this.validacion(validacion.campo)) {
        return this.alertValidacionicompleta(validacion);
      }
    }
    return true;
  }

  validarHistoriaClinica() {
    const validaciones = [
      { funcion: this.validarMotivoConsulta.bind(this), mensaje: "Llena el motivo de la consulta" },
      { funcion: this.validarEvolucionDelPaciente.bind(this), mensaje: "Llena la evolución del paciente" },
      { funcion: this.validarPlan.bind(this), mensaje: "Completa el plan" }
    ];
    for (let validacion of validaciones) {
      if (!validacion.funcion()) {
        alerta2('warning', validacion.mensaje)
        return false;
      }
    }
    return true;
  }

  // agregamos la hoja de evlucion a la consulta
  agregarHistoriaClinica(){
    this.spinner.show();
    if(this.validarHistoriaClinica() && this.validarAparatosYsistemas() == true){
      this._HistoriaClinicaService.agregarHistoriaCLinicaPaciente(Object.assign(this.hojaEvolucion,{idPaciente:this.paciente2._id}))
      .subscribe((data)=>{
        let idHistoriaclinica = { idHistoriaClinica: data['data']['_id'] }
        if (idHistoriaclinica !== undefined) {
          this.hojaEvolucion.status = "Receta"
          this._HistoriaClinicaService.agregarHojaEvolucion( this.id, (Object.assign(this.hojaEvolucion, {idHistoriaClinica: idHistoriaclinica.idHistoriaClinica})))
          .subscribe( (data) => {
            if(data['ok']){ 
              this._HistoriaClinicaService.actualizarHistoriaclinicaPorAparatos(  this.paciente.idPaciente, idHistoriaclinica )
              .subscribe( (data) => {
                if(data['ok']){
                  this.spinner.hide();
                  this.router.navigate(['receta','medica', this.id ]) 
                }
              });
            }
          }); 
        }
      })
    }
    this.spinner.hide();
    return;
  }

  public idContentB: number = 0;
  selectContentB(index: number) {
    this.idContentB = index; 
  }

  public idContentC: number = 0;
  selectContentC(index: number) {
    this.idContentC = index;
  }
}
