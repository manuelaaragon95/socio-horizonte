import { Component, OnInit } from '@angular/core';
import { WsLoginService } from 'src/app/services/sockets/ws-login.service';
import {  ConsultaService } from 'src/app/services/consultas/consulta.service';

import * as moment from 'moment';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bitacora',
  templateUrl: './bitacora.component.html',
  styleUrls: ['./bitacora.component.css']
})
export class BitacoraComponent implements OnInit {

  public consultas:[] =[];
  public fecha:String;
  public nombreDoctror="";
  public totalpaciente:string;
  public pagina = 0;
  public imprimir = {
    indice: 0,
    fecha: '',
    hora:  '',
    nombre: '',
    edad: '',
    sexo: '',
    diagnostico: '',
    curp:'',
    enfermeria:'',
    observaciones:''
  }
  public noSe = [];

  constructor(private _consultaService: ConsultaService,
              private _wsLognService: WsLoginService,
              private _router:Router){}

  ngOnInit(): void {
    this.fecha = moment().format('L');
    this.obtenerConsultasMedico();
    this._wsLognService.escucharBitacoraDoctor()
    .subscribe(arg => {
      if(arg){
        this.obtenerConsultasMedico();
      }
    });
  }

  obtenerNombreDoctor(){
    this.nombreDoctror = JSON.parse(localStorage.getItem('usuario')).nombre
  }


  obtenerConsultasMedico(){
    this.obtenerNombreDoctor();
    this._consultaService.verConsultasMedico( this.nombreDoctror )
    .subscribe( (data) => {
      data['data'].forEach(element => {
        if (element.paciente != null) {
          this.consultas = data['data'].reverse();
          this.totalpaciente = data['data'].results;
          this.setPaciente(data['data']);
        }
      });
    })
  }


  cambiarEstado(id: string, consulta) {
    let estado = {
      status:'Medico'
    }
    this._consultaService.cambiarEstadoConsulta(id, estado)
    .subscribe(data => console.log(data));

    if(consulta === 1 || consulta == 0){
      this._router.navigateByUrl('/historia/clinica/'+id);
    }else if(consulta > 1){
      this._router.navigateByUrl('/hoja/evolucion/'+id);
    }
  }

  setPaciente(data) {
    let i= 0;
    for (let element of data) {    
      this.imprimir.indice= i + 1;
      this.imprimir.fecha= element.fechaIngreso,
      this.imprimir.hora= element.horaIngreso,
      this.imprimir.nombre= element.paciente.nombrePaciente,
      this.imprimir.edad= element.paciente.edad,
      this.imprimir.sexo= element.paciente.genero,
      this.imprimir.diagnostico= element.diagnosticoActual,
      this.imprimir.curp= element.paciente.curp,
      this.imprimir.enfermeria= element.enfermeraAtendio,
      this.imprimir.observaciones= element.motivoIngreso
      this.noSe.push(this.imprimir)
      this.imprimir = {
        indice: 0 ,
        fecha: '',
        hora:  '',
        nombre: '',
        edad: '',
        sexo: '',
        diagnostico: '',
        curp:'',
        enfermeria:'',
        observaciones:''
      }
      i++
    }
  }

  imprimirBitacora(){
    let values: any;
    values = this.noSe.map((elemento) => Object.values(elemento));
    const doc:any = new jsPDF();
    doc.autoTable({
      head: [['#', 'Fecha', 'Hora', 'Nombre', 'Edad', 'Sexo', 'Diagnostico', 'Curp', 'Enfermeria', 'Observaciones' ]],
      body: values
    })
    doc.save('Bitácora_De_Consulta_Externa_'+this.fecha+'_.pdf')
  }

  /* imprimirBitacora(){

    const doc:any = new jsPDF();

    doc.autoTable({ html: "#bitacora" });

    doc.save( `Bitácora medicina general${this.fecha}.pdf`);
  } */


}
