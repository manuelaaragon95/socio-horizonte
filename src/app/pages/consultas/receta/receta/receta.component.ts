import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HistoriaClinicaService } from 'src/app/services/historiaClinica/historia-clinica.service';
import * as moment from 'moment';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { getDataStorage } from 'src/app/functions/storage/storage.functions';
import { MedicamentosService } from 'src/app/services/farmacia/medicamentos.service';
import { IntegradosService } from 'src/app/services/servicios/integrados.service';
import { NgxSpinnerService } from "ngx-spinner";
import { alerta, alerta2 } from 'src/app/clases/helpers/alerta';

@Component({
  selector: 'app-receta',
  templateUrl: './receta.component.html',
  styleUrls: ['./receta.component.css']
})
export class RecetaComponent implements OnInit {

  public fecha: string;
  public hora: string;
  public id: string;
  public nombresMedicamentosDB:[];
  public salOSustanciaDB:[];


  public medico = {
    idMedico: '',
    nombre: '',
    cedulaProfesional: '',
    Especialidad: '',
    cedulaEsp:''
  }

  public estudios:[] =[];

  public buscarEstudiotxt = {
    estudio:""
  }


  public medicamentoServiceDoctor = {
    nombreComercial:"",
    nombreDeSalOsustanciaActiva:"",
    presentacio:"",
    contenidoFrasco:"",
    viaDeAdministracion:""
  }

  public recetaPDF= {
      height:3.7,
      width:1.5,
      whithEspacio: 3.4
  }

  public paciente = {
    nombre: '',
    apellidoPaterno: '',
    apellidoMaterno: '',
    _id: '',
    edad: '',
    curp: '',
    registro: '',
    genero: '',
    medico: '',
    calle: '',
    municipio: '',
    talla: '',
    peso: '',
    imc: '',
    fc: '',
    fr: '',
    temp: '',
    pc: '',
    pa: '',
    pt: '',
    apgar: '',
    sao: '',
    alergias: '',
    callePaciente: "",
    membresiaActica: false
  }

  public paciente2 = {
    nombrePaciente:"",
    apellidoPaterno:"",
    apellidoMaterno: "",
    fechaNacimientoPaciente:"",
    curp:"",
    telefono:0,
    consultas: 0,
    _id:"",
    fechaRegistro:Date,
    genero:"",
    estadoPaciente:"",
    callePaciente:"",
    paisPaciente:"",
    cpPaciente:"",
    contactoEmergencia1Nombre:"",
    contactoEmergencia1Edad:"",
    contactoEmergencia1Telefono:"",
    correo: "",
    edad:"",
    paquetes : [],
    membresiaActiva:false,
    numeroExpediente:''
  }

  public busuqedaMedicamento ="";

  public medicamentos = [];
  public diagnostico = []
  public talla: any
  public peso:any
  public temp:any


  public indicaciones = {
    medicmanento: '',
    indcacionesMedicamento: ''    
  }

  public recetMedica = {
    medicamentos: [],
    estudios: [],
    otrasIndicaciones: "",
    idPaciente: "",
    idConsulta: "",
    fechaReceta: '',
    horareceta: '',
    medicoQueAtendio: '',
    idMedico: '',
    cedula: '',
    especialidad:'',
    cedulaEsp:'',
    firma: '',
    origen: '',
    horaEgreso: '',
    prioridad: 'Programado'
  }

  constructor(private _route: ActivatedRoute,
              private _historiaClinicaService: HistoriaClinicaService,
              private _medicamentosService: MedicamentosService,
              public _consultaService: ConsultaService,
              public _router: Router,
              public _integradosService: IntegradosService,
              private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    this.obtenerSede();
    this.fecha = moment().format('L');
    this.hora = moment().format('LT');
    this.id = this._route.snapshot.paramMap.get('id');
    this.obetenerConsulta();
    this.getMedicoData();
  }

  obtenerSede(){
    this.recetMedica.origen = localStorage.getItem('cede');
  }

  getMedicoData() {
    this.medico.nombre = getDataStorage().nombre
    this.medico.idMedico = getDataStorage()._id;
    this.medico.cedulaProfesional = getDataStorage().cedulaProfesional;
    this.medico.Especialidad = getDataStorage().Especialidad;
    this.medico.cedulaEsp = getDataStorage().cedulaEsp;
  }

  setRecetaDataMedico(){
    this.recetMedica.medicoQueAtendio = this.medico.nombre;
    this.recetMedica.idMedico = this.medico.idMedico;
    this.recetMedica.cedula = this.medico.cedulaProfesional;
    this.recetMedica.especialidad = this.medico.Especialidad;
    this.recetMedica.cedulaEsp = this.medico.cedulaEsp;
    this.recetMedica.fechaReceta = this.fecha;
    this.recetMedica.horaEgreso = moment().format('LT');
  }

  obetenerConsulta(){
    this._historiaClinicaService.obtenerConsultaPorElId(  this.id)
    .subscribe(  (data:any) => {
      this.paciente.nombre = data['data'].paciente.nombrePaciente;
      this.paciente.apellidoPaterno = data['data'].paciente.apellidoPaterno;
      this.paciente.apellidoMaterno = data['data'].paciente.apellidoMaterno;
      this.paciente.registro = data['data']['paciente']['fechaRegistro'];
      this.paciente.calle = data['data'].paciente.callePaciente;
      this.paciente.edad = data['data']['paciente']['edad'];
      this.paciente.municipio = data['data']['paciente']['estadoPaciente'];
      this.paciente.genero = data['data']['paciente']['genero'];
      this.paciente.curp = data['data']['paciente']['curp'];
      this.paciente._id = data['data']['paciente']['_id'];
      this.paciente.membresiaActica = data['data']['paciente']['membresiaActiva'];
      this.paciente.callePaciente = data['data']['paciente']['callePaciente'];
      this.talla = data['data'].talla
      this.peso = data['data'].peso
      this.temp = data['data'].temp
      this.diagnostico = data['data'].diagnosticos
      this.setPaciente(data['data'].paciente);
    });
  }

  setPaciente(data){
    for (const key in this.paciente2) {
      if (data[key] == undefined) {
        this.paciente2[key] = ''
      }else{
        this.paciente2[key] = data[key]
      }
    }
  }

  buscarMedicamento() {
    if(this.busuqedaMedicamento.length == 0 ){
      this.medicamentos = [];
    }
    if(this.busuqedaMedicamento.length >= 4) {
      this._medicamentosService.obtenerMedicamentoPorNombre(this.busuqedaMedicamento)
      .subscribe((data) => {
        this.nombresMedicamentosDB = data['data'][0];
        this.salOSustanciaDB = data['data'][1];
        this.medicamentos = this.nombresMedicamentosDB.concat(this.salOSustanciaDB);
      });
    }
  }

  deleteItem(i:number){
    this.recetMedica.medicamentos.splice( i, 1 );
  } 

  validarMedicamento(){
    if( this.medicamentoServiceDoctor.contenidoFrasco == "" && this.medicamentoServiceDoctor.nombreComercial == "" && this.medicamentoServiceDoctor.nombreDeSalOsustanciaActiva == "" && this.medicamentoServiceDoctor.presentacio == "" && this.medicamentoServiceDoctor.viaDeAdministracion == "" ){
      alerta('error', 'El medicamento esta incompleto', 'Completa los campos');
      return false;
    }
    return true;
  }

  agregarMedicamentosDesdeDoctor() {
    if(this.validarMedicamento()){
      this._medicamentosService.agragarmedicamentos(this.medicamentoServiceDoctor).subscribe((data) => { 
        if (data['ok']){
          alerta2('success', 'LOS MEDICAMENTOS SE AGREGARON CORRECTAMENTE');
          this.resetMedicamentos();
        } 
      });
    }
  }

  public resetMedicamentos (){ 
    this.medicamentoServiceDoctor.contenidoFrasco = "";
    this.medicamentoServiceDoctor.nombreComercial = "";
    this.medicamentoServiceDoctor.nombreDeSalOsustanciaActiva = "";
    this.medicamentoServiceDoctor.presentacio = "";
    this.medicamentoServiceDoctor.viaDeAdministracion = "";
  }

  setMedicamentos(event: HTMLElement) {
    this.indicaciones.medicmanento = event.textContent;
    this.busuqedaMedicamento = this.indicaciones.medicmanento;
    this.medicamentos = [];
  }

  agregarMedicamentosEIndicaciones( ){
    if(  this.validarOtrasIndicaciones()  ){ 
      let indicaciones = {
        medicamento: this.indicaciones.medicmanento,
        indicaciones: this.indicaciones.indcacionesMedicamento,
      }
      this.recetMedica.medicamentos.push(indicaciones);
      this.busuqedaMedicamento = "";
      this.resetearIndicaciones();
    }else{ 
      return;
    }
  }

  resetearIndicaciones() {
    this.indicaciones.medicmanento ="";
    this.indicaciones.indcacionesMedicamento="";
    this.medicamentos = [];
  }

  medicamentosRE(medicamentos, indicaciones) {
    this.indicaciones.medicmanento =  medicamentos,
    this.indicaciones.indcacionesMedicamento = indicaciones
  }

  setIDconsulta(){
    this.recetMedica.idConsulta = this.id;
  }

  setIdPaceinte() {
    this.recetMedica.idPaciente = this.paciente._id;
  }

  agregarReceta() {
    this.setIDconsulta();
    this.setIdPaceinte();
  }

  setIds() {
    this.recetMedica.idPaciente = this.paciente._id;
    this.recetMedica.idConsulta = this.id;
  }

  validarOtrasIndicaciones(){
    if(this.indicaciones.medicmanento == '') {
      alerta('warning','','SELECCIONA UN MEDICAMENTO ANTES DE GUARDAR');
      return false;
    } else if( this.indicaciones.indcacionesMedicamento == '' ){
      alerta('warning', '', 'COMPLETA LAS INDICACIONES MEDICAS');
     return false;
    } 
    return true;
  }

  guardarReceta(){
    this.spinner.show();
    this.setIds();
    this.setRecetaDataMedico();
    if(this.recetMedica.medicamentos.length == 0){
      this.spinner.hide();
      alerta2('warning', 'Debes agregar un medicamento');
      return;
    }
    this._consultaService.agregarReceta(  this.recetMedica )
    .subscribe( data => {
      if(data['ok']){
        let estado = {
          status : 'Receta'
        }
        this._consultaService.cambiarEstadoConsulta( this.id,  estado  )
        .subscribe( data  => {
          if(data['ok']){ 
            this.spinner.hide();
            alerta2('success', 'LA RECETA SE GUARDO CORRECTAMENTE');
            this._router.navigateByUrl('/bitacora/medicina/general');
          }
        });
      }
    });
  }

  eliminarEstudio( id: number ){
    this.recetMedica.estudios.splice(id, 1);
  }

  // bisca los estudios en al base de datos
  buscarEstudio() {
    if( this.buscarEstudiotxt.estudio.length == 0 ){
      this.estudios = []
    }
    if (this.buscarEstudiotxt.estudio.length > 5) {
      this._integradosService.getAllDepartments( this.buscarEstudiotxt )
      .subscribe( data => {
        this.estudios = data['data'][1]
      });
    }
  }

  // agrega los estudio al json y a la interfaz
  agregarEstudio(estudio){
    this.recetMedica.estudios.push(  estudio );
    this.buscarEstudiotxt.estudio = "";
    this.estudios = [];
  }

  imprimirFrente() {
    const tiempoTranscurrido = Date.now();
    const hoy = new Date(tiempoTranscurrido);
    /*HOJA POR EL FRENTE*/
    let imgSocios = '../../assets/images/socio_horizonte.png';
    let linea1 = '../../assets/images/linea 1.png';
    let linea2 = '../../assets/images/linea 2.png';
    let hora_impresion = hoy.toLocaleDateString() 
    const doc:any = new jsPDF({
      unit: "cm",
      format: 'a4',
    });
    doc.addImage(imgSocios, 'PNG', 1, 0.5,6,2);
    doc.addImage(linea2, 'PNG', 10, 1);
    doc.setFontSize(8);
    doc.setFont('helvetica')
    doc.text('RECETA MEDICA:', 10, 2.5)
    doc.text('FECHA DE CONSULTA:', 14, 3.5)
    doc.text(`${hora_impresion}`,17.5,3.5)
    doc.text('NOMBRE:', 1, 4)
    doc.text(`${this.paciente.nombre} ${this.paciente.apellidoPaterno} ${this.paciente.apellidoMaterno}`, 2.6, 4)
    doc.text('EDAD: ', 10, 4);
    doc.text(`${this.paciente.edad}` + ' AÑOS', 11, 4);
    doc.text('GÉNERO: ', 15, 4);
    doc.text(`${this.paciente.genero}`, 17, 4);
    doc.text('PESO:', 1, 4.5)
    doc.text(`${this.peso}` + ' Kg', 2.6, 4.5)
    doc.text('TALLA: ', 10, 4.5);
    doc.text(`${this.talla}` + ' M', 11, 4.5);
    doc.text('TEMP.: ', 15, 4.5);
    doc.text(`${this.temp}` + ' °C', 17, 4.5);
    doc.text('IDX:', 1, 5)
    this.diagnostico.forEach((element,imdex) => {
      doc.text(`${element.diagnostico}`, 2.6, 5)
    });
    doc.text('MEDICAMENTOS:', 1, 6.3)
    let a = 6.8;
    let b = 7.2;
    this.recetMedica.medicamentos.forEach((medicamento: any, index) => {
      this.recetaPDF.height += 1;
      doc.text(1, a, (this.recetaPDF.width, this.recetaPDF.height, (index+1) + '.-'+`${medicamento.medicamento}`));
      a = a + 1.3;
      this.recetaPDF.height += 1;
      doc.text(1, b, (this.recetaPDF.width, this.recetaPDF.height, `${medicamento.indicaciones.toUpperCase()}`));
      b = b + 1.3;
    });
    doc.text('OTRAS INDICACIONES:', 1, (b+0.5))
    doc.text(`${this.recetMedica.otrasIndicaciones}`, 1, (b+1.3), { maxWidth: 15 })
    doc.text('ESTUDIOS:', 1,20)
    let c = 20.5
    this.recetMedica.estudios.forEach((estudio, i) => {
      doc.text(1, (c+0.5), `${estudio.ESTUDIO}`);
      c = c + 1.3;
    })

    doc.text("Doctor: " + this.recetMedica.medicoQueAtendio, 10,(c+0.5));
    doc.text("Especialidad: " + this.recetMedica.especialidad, 10, (c+1));
    doc.text("Cédula: " + this.recetMedica.cedula + "/" + this.recetMedica.cedulaEsp, 10,(c+1.5));
    doc.text("Dirección: " + this.recetMedica.cedula, 10,(c+2));
    doc.addImage(linea1, 'PNG', 0, 28.0);
    //doc.output('dataurlnewwindow');
    doc.save(this.paciente.nombre + " " + this.paciente.apellidoPaterno);
  }
}
