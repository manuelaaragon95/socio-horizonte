export interface Estudios{
    data:Array<Estudio>,
    message: string,
    ok: boolean
}

export interface Estudio{
    countServices:number,
    createdAt:string,
    doctorQueSolicito:string,
    fechaPedido:string,
    hora:string,
    idPaciente: string,
    idServicio:Servicio,
    idVenta:string,
    manzo:boolean,
    nombreServicio:string,
    pdf?:string,
    link?:string,
    pedidosSedes:boolean,
    recibe:string,
    resultados:Array<any>,
    sede:string,
    status:string,
    updatedAt:string,
    urgencia:boolean
}

export interface Servicio{
    _id: string, 
    ESTUDIO: string
}