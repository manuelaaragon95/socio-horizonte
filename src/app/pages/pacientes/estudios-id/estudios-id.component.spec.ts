import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EstudiosIdComponent } from './estudios-id.component';

describe('EstudiosIdComponent', () => {
  let component: EstudiosIdComponent;
  let fixture: ComponentFixture<EstudiosIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EstudiosIdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EstudiosIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
