import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import Pacientes from 'src/app/interfaces/pacientes.interface';
import { PacientesService  } from '../../../services/pacientes/pacientes.service';
import Swal from 'sweetalert2';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { NgxSpinnerService } from "ngx-spinner";
import { buscarPaciente } from '../../../clases/helpers/filterNamePaciente';
 
/* import { SpinnerService } from 'src/app/services/spinner/spinner.service'; */

@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit, OnDestroy {

  public pacientes2:any[] = [];
  public pacientes3:any[]=[]
  public totalpaciente:string;
  public pagina = 0;
  public  filtropacientes = '';
  public  filtroexpediente = '';
  public filterPost = '';
  public sede = '';
  public idSede = '';
  public rol='';
  public pacientes =  [];
  public idUsuario = '';
  public validar = false;
  private unsubcribe$ = new Subject<void>();

  constructor(public _pacienteService: PacientesService,
              private _spinnerService:SpinnerService,
              private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    this.sede = localStorage.getItem('cede')
    this.idSede = localStorage.getItem('IdSede')
    this.rol = JSON.parse(localStorage.getItem('usuario')).role;
    this.idUsuario = JSON.parse(localStorage.getItem('usuario'))._id
    this.obtenerPacientes();
    this.focusSearchPatients();
    this.validarUsuario();
    this.spinner.show();
  }

  validarUsuario(){
    if(this.idUsuario = '5fdcdf47eabf6e0017eee246'){
      this.validar = true;
    }
  }

  obtenerPacientes() {
    this._pacienteService.getPacientesSede(this.sede)
    .subscribe( (data: any) => {
      console.log(data);
      
      if( data['message']   === 'No hay pacientes' ) {
         Swal.fire({
              icon: 'success',
              title: '',
              text: 'NO SE ENCUENTRA NINGUN PACIENTE',
            })
        return;
      }
      this.pacientes2 = data.data;
      this.pacientes = data.data;
      this.totalpaciente = data.data.results;
      this.spinner.hide();
    });
  }

  typeSearchPatients(){
    if(this.filtropacientes.length >= 3){
      this.pagina = 0;
      this._pacienteService.buscarPacienteSede(this.sede ,{nombre: this.filtropacientes} )
      .subscribe( (data: any) => {
        this.pacientes2 = data.data
      });
    }else{
      this.pagina = 0;
      this.pacientes2 = this.pacientes;
    }
  }

  typeSearchExpedientes(){
    if(this.filtroexpediente.length > 0){
      this._pacienteService.buscarPacientesPorExpedienteBackend( {expediente: this.filtroexpediente} )
      .subscribe( (data: any) => this.pacientes2 = data.data);
    }else{
      this.pacientes2 = this.pacientes;
    }
  }

  // buscarTodosPacientes(){
  //   this._pacienteService.getPacientesAll()
  //   .subscribe((resp:any)=>{
  //     this.pacientes3 = resp['data'].reverse();
  //   })
  // }

  focusSearchPatients(){
    const input: HTMLInputElement = document.querySelector('#busquedaPaciente');
    input.select();
  }

  paginaAnterior(){
    this.pagina = this.pagina - 8;
    this._pacienteService.getPacientes( this.pagina).subscribe( (data: any) => {
      this.pacientes2 = data.users;
    })
  }

  siguentePagina(){
    this.pagina += 8;
    this._pacienteService.getPacientes( this.pagina).subscribe( (data: any) => {
      this.pacientes2= data.users;
    })
  }

  ngOnDestroy(){
    this.unsubcribe$.next();
    this.unsubcribe$.complete();
  }
}
