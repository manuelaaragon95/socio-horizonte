import { Component, OnInit } from '@angular/core';
import { PagosService } from 'src/app/services/pagos/pagos.service';
import { SedesService } from 'src/app/services/ventas/sedes.service';
import { NgxSpinnerService } from "ngx-spinner";
import { alerta2 } from 'src/app/clases/helpers/alerta';
// import Swal from 'sweetalert2'

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  public pagos = [];
  public total=0;
  public totalUtilidad=0;
  public util=[];
  public utilidad2=[];
  public monto=[];
  public utilidadesCompra =[];
  public gananciaTotal = 0;

  public fechas = {
    fecha1:"",
    fecha2:"",
    sede: ""
  }
  ganancias: number = 0;
  public fechasPedidos:any[]=[];

  constructor(private _sedesServices: SedesService,
              private _pagosService: PagosService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.obtenerSede();
    this.obetenerPagos();
  }

  obtenerSede(){
    this.fechas.sede =  localStorage.getItem('cede');
  }

  obetenerPagos(){
    this.spinner.show();
    this._sedesServices.verPedidosPagados(this.fechas.sede)
    .subscribe(  (data) => {
      this.setPagos( data['data'] );
      this.setGanancia( data['data'] );
      this.spinner.hide();
    });
  }

  setPagos(pagos) {
    this.pagos = pagos.filter(element => element.paciente != null);
    this.total = 0;
    this.monto = [];
    this.utilidad2 = [];
    this.pagos.forEach(pago => {
        this.total += pago.totalCompra;
        this.monto.push(pago.montoAnticipo);
        if (pago.estudios.length === 1) {
            this.utilidad2.push(pago.estudios[0].utilidad);
        } else {
            const utilidad = pago.estudios.reduce((sum, estudio) => sum + estudio.utilidad, 0);
            this.utilidad2.push(utilidad);
        }
    });
    this.obtenerMontos(this.monto, this.utilidad2);
    this.pagos.reverse();
  }

  setGanancia(ganancias = []) {
    this.gananciaTotal = ganancias.reduce((total, element) => {
        return element.ganancia !== undefined ? total + element.ganancia : total;
    }, 0);
  }

  obtenerMontos(monto, utilidad){
    for (let i = 0; i < utilidad.length; i++) {
      const element = utilidad[i];
      this.util.push(monto[i]);
    }

    for (let x = 0; x < this.util.length; x++) {
      const element = this.util[x];
      this.ganancias += element;
    }
    return this.util;
  }

  buscarPorFechas(){
    if( this.fechas.fecha1 == "" && this.fechas.fecha2 == ""  ){
      alerta2('warning', 'Ingresa un rango de fechas');
      return
    }
    this.spinner.show();
    this._pagosService.buscarPorFechasPagos(this.fechas)
    .subscribe(  data => {
      this.setPagos( data['data'] );
      this.setGanancia( data['data'] );
      this.spinner.hide();
    })
  }

}
