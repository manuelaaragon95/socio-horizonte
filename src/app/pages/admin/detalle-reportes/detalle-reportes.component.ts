import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SedesService } from 'src/app/services/ventas/sedes.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-detalle-reportes',
  templateUrl: './detalle-reportes.component.html',
  styleUrls: ['./detalle-reportes.component.css']
})
export class DetalleReportesComponent implements OnInit {

  public sede = '';
  public id = '';
  public pedidos={
    fecha:''
  }
  public estudios:any [] = []
  
  constructor(public _route: ActivatedRoute,
              public _sede: SedesService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.sede = localStorage.getItem('cede')
    this.id =  this._route.snapshot.paramMap.get('id');
    this.obtenerDetalle();
  }

  obtenerDetalle() {
    this.spinner.show();
    this._sede.verDesglosePedido(this.sede, this.id).subscribe((resp: any) => {
      if (resp.ok) {
        const [data] = resp['data'] || [];
        this.estudios = this.identificarEstructura(data?.estudios);
        this.pedidos = data;
        this.spinner.hide();
      }
    });
  }
  
  identificarEstructura(estudios: any[]) {
    return estudios?.map(estudio => ({
      nombreEstudio: estudio?.nombreEstudio || estudio?.idServicio?.ESTUDIO || ""
    })) || [];
  }  
}
