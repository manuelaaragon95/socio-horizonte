export interface paquete {
    consultas:[],
    costoTotal:0,
    descuentos?:[],
    extras?:[],
    farmacia?:[],
    icon:'',
    laboratorio?:[],
    nombrePaquete:'',
    rayosX?:[],
    ultrasonido?:[]
}

export interface venta {
    paciente:string,
    nombrePaciente:string,
    vendedor:string,
    hora:string,
    estudios:any[],
    efectivo:boolean,
    doctorQueSolicito:string,
    transferencia: boolean,
    tarjetCredito:boolean,
    tarjetaDebito:boolean,
    montoEfectivo:number,
    montoTarjteCredito:number,
    montoTarjetaDebito:number,
    montoTranferencia: number,
    sede:string,
    totalCompra:0,
    prioridad:string,
    compraConMembresia:boolean,
    status:'Pagado',
    fecha:string
}

export interface ventaPaquetes {
    paciente: string,
    fecha: string,
    hora: string,
    paquete: string,
    anticipo:false,
    totalCompra?:number,
    estudios?:any[],
    sede:string,
    nombrePaciente:string,
    vendedor:string,
    efectivo:false,
    doctorQueSolicito:string,
    transferencia: false,
    tarjetCredito:false,
    tarjetaDebito:false,
    montoEfectivo?:number,
    montoTarjteCredito?:number,
    montoTarjetaDebito?:number,
    montoTranferencia?:number,
    prioridad:string,
    compraConMembresia:true,
    status:'Pagado',
    ganancia?:number,
    ganancias?:number
}

export interface carrito {
    totalSin: 0,
    totalCon: 0,
    items:any[]
}

export interface consulta {
    nombre:string,
    tipo: string, 
    consulta: string, 
    fecha: string, 
    hora:string, 
    medico:string, 
    firma: string
}