export interface CarritoNuevo {
    totalSin: number,
    totalCon: number,
    items:Estudio[]
}

export interface CarritoMembresia{
    items:[],
    total:0
}

interface Estudio{
    nombreEstudio: string,
    precioSin:string,
    precioCon:string,
    name: string,
    idEstudio: string
}