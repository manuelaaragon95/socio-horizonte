
export default class Pacientes {
    constructor(
     public  nombre: String,
     public  apellidoPaterno: String,
     public  apellidoMaterno: String,
     public  estadoPaciente: String,
    
     public  fechaNacimiento: String,
     public  telefono: String,
     public  edad: String,
     public  genero: String,
      public curp:String,
      public callePaciente:String,
      
     ) {
  
    }
    
  }
  export interface Paciente{
    nombrePaciente:string,
    apellidoPaterno:string,
    apellidoMaterno: string,
    fechaNacimientoPaciente:string,
    curp:string,
    telefono:0,
    consultas: 0,
    _id:string,
    fechaRegistro:Date,
    genero:string,
    estadoPaciente:string,
    callePaciente:string,
    paisPaciente:string,
    cpPaciente:string,
    contactoEmergencia1Nombre:string,
    contactoEmergencia1Edad:string,
    contactoEmergencia1Telefono:string,
    correo: string,
    edad:string,
    membresiaActiva:false,
    numeroExpediente:'',
    sede:string
  }

  export interface pacienteCompleto {
    apellidoMaterno: ""
    apellidoPaterno: ""
    callePaciente: ""
    numeroPaciente:""
    numeroIntPaciente:""
    paisPaciente:""
    cpPaciente:""
    municipio:""
    municipioPaciente:""
    colonia:""
    contactoEmergencia1ApellidoMaterno: ""
    contactoEmergencia1ApellidoPaterno: ""
    contactoEmergencia1Edad: ""
    contactoEmergencia1EstadoCivil: ""
    contactoEmergencia1Genero: ""
    contactoEmergencia1Nombre: ""
    contactoEmergencia1Parentesco: ""
    contactoEmergencia1Telefono: ""
    contactoEmergencia1Correo:""
    correoPaciente: ""
    cpRazonSocial: ""
    curp: ""
    disponente: ""
    edad: number
    paisNacimiento:""
    estadoCivilPaciente:""
    entidadNacimiento: ""
    estadoPaciente: ""
    fechaNacimientoPaciente: string
    genero: ""
    lugardeorigen: ""
    membresiaActiva: false
    nombrePaciente: ""
    nomenclaturaRegistro: ""
    numeroExpediente: ""
    razonSocial1:""
    razonSocial1RFC:""
    correoRazonSocial1:""
    razonSocial1Calle: ""
    razonSocial1NumInt:""
    razonSocial1NumExt:""
    razoncocial1RFC: ""
    razonSocial1Estado:""
    razonSocial1Municipio:""
    razonSocial1colonia:""
    razonsocial1Telefono:""
    razonSocialPais:""
    referenciaPaciente: ""
    referenciaPaciente2: ""
    sede: ""
    status: ""
    telefono: 0
    estadoCivilPciente:""
  }
  